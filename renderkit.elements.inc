<?php

/**
 * @file
 * Contains element types.
 */

use Drupal\renderkit\R;

/**
 * Implements hook_element_info().
 */
function renderkit_element_info() {
  $elements = [];
  // Elements with '#type' => 'renderkit_generic_form' will be converted into
  // forms.
  // The form id will be either 'renderkit__' . $element['#form_id_suffix'], or
  // just '_renderkit_generic_form', if no suffix is specified.
  $elements['renderkit_generic_form'] = [
    '#pre_render' => [R::f('_renderkit_generic_form_pre_render')],
  ];
  // Register this element type even if 'ds' module is not enabled.
  // Instead, let the callback check if 'ds' is enabled.
  $elements['renderkit_ds_layout'] = [
    '#pre_render' => [R::f('_renderkit_ds_layout_pre_render')],
  ];
  return $elements;
}

/**
 * The '#pre_render' callback for 'renderkit_generic_form' element.
 *
 * Transforms a regular render element into a form.
 *
 * @param array $element
 *   Original render element.
 *
 * @return array
 *   Transformed render element.
 */
function _renderkit_generic_form_pre_render(array $element) {

  // Check if the element was already transformed.
  if (!isset($element['#type']) || $element['#type'] !== 'renderkit_generic_form') {
    // The element was already transformed.
    return $element;
  }

  // Remove everything that would cause a second transformation.
  unset($element['#type']);
  if (isset($element['#pre_render'])) {
    foreach (array_keys($element['#pre_render'], __FUNCTION__) as $i) {
      unset($element['#pre_render'][$i]);
    }
    if (empty($element['#pre_render'])) {
      unset($element['##pre_render']);
    }
  }

  // Determine a form id.
  /* @see \renderkit_forms() */
  $form_id = isset($element['#form_id_suffix'])
    ? 'renderkit__' . $element['#form_id_suffix']
    : R::f('_renderkit_generic_form');

  // Convert the element into a form.
  return drupal_get_form($form_id, $element);
}

/**
 * Element '#pre_render' callback for 'renderkit_ds_layout' element type.
 *
 * This is a revamped version of ds_entity_variables() as found in ds.
 *
 * @param array $element
 *   Original render element.
 *
 * @return array
 *   Processed render element.
 *
 * @throws \Exception
 *   In theme(), if too early in the request.
 *
 * @see \ds_entity_variables()
 * @see \ds_field_attach_view_alter()
 */
function _renderkit_ds_layout_pre_render(array $element) {
  if (!module_exists('ds')) {
    watchdog(
      'renderkit',
      'Display Suite is not installed. Regions will be rendered without layout markup.',
      [],
      WATCHDOG_WARNING);
    return $element;
  }

  $layout_name = $element['#layout_name'];
  $layouts = ds_get_layout_info();

  if (!isset($layouts[$layout_name])) {
    watchdog(
      'renderkit',
      'Layout @layout not found. Regions will be rendered without layout markup.',
      ['@layout' => var_export($layout_name, TRUE)],
      WATCHDOG_WARNING);
    return $element;
  }

  $layout = $layouts[$layout_name];

  // Prepare region content.
  $rendered_regions = [];
  foreach ($layout['regions'] as $region_name => $region) {
    if (0 === strncmp('#', $region, 1)) {
      // Dangerous region name.
      // @todo Log this.
      $rendered_regions[$region_name] = '';
    }
    elseif (empty($element[$region_name])) {
      $rendered_regions[$region_name] = '';
    }
    else {
      $rendered_regions[$region_name] = drupal_render($element[$region_name]);
      unset($element[$region_name]);
    }
  }

  if (isset($layout['module']) && $layout['module'] == 'panels') {
    // This is a panels layout.
    if (!empty($layout['flexible'])) {
      // This is a flexible panels layout.
      $element['#theme'] = R::th('theme_panels_flexible');
      ctools_include('plugins', 'panels');
      $element['#layout'] = panels_get_layout($layout['panels']['name']);
    }
    else {
      // This is a regular panels layout.
      $element['#theme'] = $layout['panels']['theme'];
    }

    // Add region content.
    $element['#content'] = $rendered_regions;

    // Attach CSS.
    /* @see \ds_field_attach_view_alter() */
    $element['#attached']['css'][] = $layout['path'] . '/' . $layout['panels']['css'];
  }
  else {
    // This is a ds layout.
    $element['#theme'] = 'renderkit_ds_layout';

    // Add region content.
    $element['#rendered_regions'] = $rendered_regions;

    // Some 3rd party code might need the layout array.
    $element['#layout'] = $layout;

    // Attach CSS.
    /* @see \ds_field_attach_view_alter() */
    $element['#attached']['css'][] = $layout['path'] . '/' . $layout_name . '.css';
  }

  return $element;
}
