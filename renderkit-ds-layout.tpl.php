<?php

/**
 * @file
 * Fallback template for 'renderkit_ds_layout' theme hook.
 *
 * @var string[] $rendered_regions
 *   Format: $[$region_name] = $region_content.
 * @var string $layout_name
 *   Name of a ds layout.
 * @var string $layout_wrapper
 *   Layout tag name.
 * @var string $classes
 *   Layout classes.
 * @var string $$region_name
 *   Rendered region content. Not used here.
 * @var string ${$region_name . '_wrapper'}
 *   Region tag name.
 * @var string ${$region_name . '_classes'}
 *   Region classes.
 */
?>
<<?php print $layout_wrapper; ?> class="renderkit-ds-layout renderkit-ds-layout--<?php print drupal_html_class($layout_name) . ' ' . $classes; ?>">
  <?php foreach ($rendered_regions as $region_name => $region_content): ?>
    <<?php print ${$region_name . '_wrapper'}; ?> class="renderkit-ds-layout-region--<?php print drupal_html_class($region_name) . ${$region_name . '_classes'}; ?>">
      <?php print $region_content; ?>
    </<?php print ${$region_name . '_wrapper'}; ?>>
  <?php endforeach; ?>
</<?php print $layout_wrapper; ?>>
