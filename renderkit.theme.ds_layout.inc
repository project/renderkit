<?php

/**
 * @file
 * Contains preprocess for 'renderkit_ds_layout' theme hook.
 */

/**
 * Theme preprocess callback for 'renderkit_ds_layout'.
 *
 * @param array $variables
 *   Theme variables.
 */
function template_preprocess_renderkit_ds_layout(array &$variables) {

  $settings = $variables['layout_settings'];

  $variables['theme_hook_suggestions'][] = $variables['layout_name'];

  $attributes = $variables['attributes'];
  unset($variables['attributes']);

  // Add a layout wrapper.
  $variables['layout_wrapper'] = isset($settings['layout_wrapper'])
    ? $settings['layout_wrapper']
    : 'div';

  // Prepare layout classes.
  // Remove automatically added classes from template_preprocess().
  $variables['classes_array'] = [];

  // Add layout classes from settings.
  if (isset($settings['classes']['layout_class'])) {
    foreach ($settings['classes']['layout_class'] as $layout_class) {
      $variables['classes_array'][] = $layout_class;
    }
  }

  // Add layout classes from $element['#attributes']['class'].
  if (!empty($attributes['class'])) {
    foreach ($attributes['class'] as $layout_class) {
      $variables['classes_array'][] = $layout_class;
    }
    unset($attributes['class']);
  }

  // Add layout attributes from settings.
  $variables['layout_attributes'] = !empty($settings['layout_attributes'])
    ? ' ' . $settings['layout_attributes']
    : '';

  // Add layout attributes from $element['#attributes'].
  $variables['layout_attributes'] .= drupal_attributes($attributes);

  if ($variables['link_path'] !== NULL) {
    // Add an onclick attribute on the wrapper.
    // @todo This should be sanitized.
    $url = url($variables['link_path']);
    $onclick = 'location.href=' . json_encode($url) . ';';
    $variables['layout_attributes'] .= ' onclick="' . check_plain($onclick) . '"';
  }

  // Prepare region settings.
  foreach ($variables['rendered_regions'] as $region_name => $region_content) {

    // Add extras classes to the region.
    $variables[$region_name . '_classes'] = !empty($settings['classes'][$region_name])
      ? ' ' . implode(' ', $settings['classes'][$region_name])
      : '';

    // Add a wrapper to the region.
    $variables[$region_name . '_wrapper'] = isset($settings['wrappers'][$region_name])
      ? $settings['wrappers'][$region_name]
      : 'div';
  }

  // Make the region content available to the template.
  $variables += $variables['rendered_regions'];
}
