<?php

/**
 * @file
 * Contains form-related hooks and logic.
 */

use Drupal\renderkit\R;

/**
 * Implements hook_forms().
 */
function renderkit_forms($form_id) {
  $forms = [];
  // Forms where the id begins with 'renderkit__' don't need a dedicated form
  // callback, but can use the 'renderkit_generic_form' mechanic instead.
  if (0 === strpos($form_id, 'renderkit__')) {
    $forms[$form_id] = [
      'callback' => R::f('_renderkit_generic_form'),
      'base_form_id' => 'renderkit_generic_form',
    ];
  }
  return $forms;
}
