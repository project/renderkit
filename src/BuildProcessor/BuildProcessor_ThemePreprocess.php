<?php

namespace Drupal\renderkit\BuildProcessor;

use Drupal\renderkit\ThemePreprocessor\ThemePreprocessorInterface;

/**
 * @deprecated
 */
class BuildProcessor_ThemePreprocess implements BuildProcessorInterface {

  /**
   * @var \Drupal\renderkit\ThemePreprocessor\ThemePreprocessorInterface
   */
  private $themePreprocessor;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\ThemePreprocessor\ThemePreprocessorInterface $themePreprocessor
   */
  public function __construct(ThemePreprocessorInterface $themePreprocessor) {
    if (!\defined('THEMEKIT_PREPROCESS')) {
      /** @noinspection PhpDeprecationInspection */
      $class = self::class;
      watchdog(
        'renderkit',
        'The component @class requires themekit-7.x-1.x.',
        ['@class' => $class]);
    }
    $this->themePreprocessor = $themePreprocessor;
  }

  /**
   * {@inheritdoc}
   */
  public function process(array $build) {
    if (!\defined('THEMEKIT_PREPROCESS')) {
      return [];
    }
    $build[THEMEKIT_PREPROCESS][] = $this->themePreprocessor;
    return $build;
  }

}
