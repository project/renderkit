<?php

namespace Drupal\renderkit\BuildProcessor;

/**
 * Object to transform a render element.
 */
interface BuildProcessorInterface {

  /**
   * Transforms a render element.
   *
   * @param array $build
   *   Render array before the processing.
   *
   * @return array
   *   Render array after the processing.
   */
  public function process(array $build);

}
