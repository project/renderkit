<?php

namespace Drupal\renderkit\EntityDisplayListFormat;

use Drupal\renderkit\ListFormat\ListFormatInterface;

/**
 * @CfrPlugin(
 *   id = "listFormat",
 *   label = @t("List format"),
 *   inline = true
 * )
 */
class EntityDisplayListFormat_ListFormat implements EntityDisplayListFormatInterface {

  /**
   * @var \Drupal\renderkit\ListFormat\ListFormatInterface
   */
  private $listFormat;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\ListFormat\ListFormatInterface $listFormat
   */
  public function __construct(ListFormatInterface $listFormat) {
    $this->listFormat = $listFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function buildListWithEntity(array $builds, $entityType, $entity) {
    return $this->listFormat->buildList($builds);
  }

}
