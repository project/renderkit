<?php

namespace Drupal\renderkit\EntityDisplayListFormat;

/**
 * Formats a list of render elements, informed by an entity.
 *
 * @todo This interface does not seem to be very useful.
 */
interface EntityDisplayListFormatInterface {

  /**
   * Formats a list of render elements, informed by an entity.
   *
   * @param array[] $builds
   *   Render elements for the list items.
   *   Array keys can be serial or associative.
   *   Array keys should not begin with '#'.
   * @param string $entityType
   *   Entity type.
   * @param object $entity
   *   Entity that should inform the formatting.
   *
   * @return array
   *   Render element to show all the items.
   */
  public function buildListWithEntity(array $builds, $entityType, $entity);

}
