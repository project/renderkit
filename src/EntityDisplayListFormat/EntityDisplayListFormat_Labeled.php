<?php

namespace Drupal\renderkit\EntityDisplayListFormat;

use Drupal\renderkit\LabeledEntityDisplayListFormat\LabeledEntityDisplayListFormatInterface;

class EntityDisplayListFormat_Labeled implements EntityDisplayListFormatInterface {

  /**
   * @var \Drupal\renderkit\LabeledEntityDisplayListFormat\LabeledEntityDisplayListFormatInterface
   */
  private $labeledEntityDisplayListFormat;

  /**
   * @var string
   */
  private $label;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\LabeledEntityDisplayListFormat\LabeledEntityDisplayListFormatInterface $labeledEntityDisplayListFormat
   * @param string $label
   */
  public function __construct(LabeledEntityDisplayListFormatInterface $labeledEntityDisplayListFormat, $label) {
    $this->labeledEntityDisplayListFormat = $labeledEntityDisplayListFormat;
    $this->label = $label;
  }

  /**
   * {@inheritdoc}
   */
  public function buildListWithEntity(array $builds, $entityType, $entity) {
    return $this->labeledEntityDisplayListFormat->build($builds,$entityType, $entity, $this->label);
  }

}
