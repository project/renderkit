<?php

namespace Drupal\renderkit\ImageProcessor;

/**
 * Transforms render elements with '#theme' => 'image'.
 *
 * The resulting element no longer needs to have '#theme' => 'image'. This means
 * that only one processor should be called for a given element.
 *
 * @todo A better name would be ImageFormatInterface.
 * @todo A render element is not the best format for the input.
 */
interface ImageProcessorInterface {

  /**
   * Transforms a render element with '#theme' => 'image'.
   *
   * @param array $build
   *   Render element with '#theme' => 'image'.
   *
   * @return array
   *   Render element after the processing.
   *   This may no longer have '#theme' => 'image'.
   */
  public function processImage(array $build);

}
