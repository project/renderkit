<?php

namespace Drupal\renderkit\ImageProcessor;

class ImageProcessor_Neutral implements ImageProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function processImage(array $build) {
    return $build;
  }

}
