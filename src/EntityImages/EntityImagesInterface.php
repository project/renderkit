<?php

namespace Drupal\renderkit\EntityImages;

/**
 * Gets a list of images from an entity.
 */
interface EntityImagesInterface {

  /**
   * Gets multiple render arrays with '#theme' => 'image' for a given entity.
   *
   * @param string $entityType
   *   The entity type.
   * @param object $entity
   *   The entity.
   *
   * @return array[]
   *   List of images encoded as render elements.
   *   Format: $[$delta] = ['#theme' => 'image', '#path' => .., ..].
   */
  public function entityGetImages($entityType, $entity);

}
