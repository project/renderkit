<?php

namespace Drupal\renderkit\FieldDisplayProcessor;

/**
 * Base class that respects the '#access' property.
 *
 * This should be used in implementations that want to completely rewrite the
 * field display render element.
 */
abstract class FieldDisplayProcessorBase implements FieldDisplayProcessorInterface {

  /**
   * {@inheritdoc}
   */
  final public function process(array $element) {

    if (!$element || (isset($element['#access']) && !$element['#access'])) {
      // The element is empty, or viewing access was denied.
      // We expect that no further alterations or processors are applied at this
      // point that would need to see the original element structure.
      // Therefore it is ok to just return empty array.
      return [];
    }

    // Apply processor logic from subclass.
    return $this->doProcess($element);
  }

  /**
   * Transforms a field render element.
   *
   * @param array $element
   *   Render element with ['#theme' => 'field', ..].
   *   The '#access' property is either TRUE or not set.
   *   The element is not an empty array, but it could be empty-ish, e.g. not
   *   contain any real content.
   *
   * @return array
   *   Processed render element.
   */
  abstract protected function doProcess(array $element);

}
