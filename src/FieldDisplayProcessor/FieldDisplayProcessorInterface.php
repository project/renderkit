<?php

namespace Drupal\renderkit\FieldDisplayProcessor;

/**
 * Transforms a field display render element.
 */
interface FieldDisplayProcessorInterface {

  /**
   * Transforms a field display render element.
   *
   * The '#access' property of the original element MUST be respected.
   * One way to do this is is to extend the FieldDisplayProcessorBase class.
   *
   * Otherwise the original element can be restructured at will.
   *
   * @param array $element
   *   Render array with ['#theme' => 'field', ..]
   *
   * @return array
   *   Processed / transformed render element.
   *   This might no longer have the typical structure of a field display render
   *   element. It should NOT be passed to another field display processor.
   *
   * @see \field_default_view()
   * @see \Drupal\renderkit\Helper\EntityTypeFieldDisplayHelper::entityBuildField()
   */
  public function process(array $element);

}
