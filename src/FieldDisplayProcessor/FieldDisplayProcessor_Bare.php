<?php

namespace Drupal\renderkit\FieldDisplayProcessor;

/**
 * Produces an element where only the bare field items will be rendered.
 *
 * This extends FieldDisplayProcessorBase, to preserve the '#access' property.
 */
class FieldDisplayProcessor_Bare extends FieldDisplayProcessorBase {

  /**
   * {@inheritdoc}
   */
  protected function doProcess(array $element) {

    $builds = [];
    foreach ($element['#items'] as $delta => $item) {
      if (!empty($element[$delta])) {
        $builds[$delta] = $element[$delta];
      }
    }

    return $builds;
  }

}
