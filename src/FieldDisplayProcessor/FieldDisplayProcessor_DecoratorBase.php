<?php

namespace Drupal\renderkit\FieldDisplayProcessor;

/**
 * Base class for decorators, to help respect the '#access' property.
 *
 * This should be used for decorators that add additional visual decorations or
 * wrappers around the main field element.
 *
 * The base class assumes that the additional decorations should not be applied
 * if the main element is empty or inaccessible.
 */
abstract class FieldDisplayProcessor_DecoratorBase implements FieldDisplayProcessorInterface {

  /**
   * @var \Drupal\renderkit\FieldDisplayProcessor\FieldDisplayProcessorInterface
   */
  private $decorated;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\FieldDisplayProcessor\FieldDisplayProcessorInterface $decorated
   */
  public function __construct(FieldDisplayProcessorInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  final public function process(array $original_element) {

    // Apply the decorated processor.
    // The decorated processor is responsible for respecting the '#access'
    // property on the original element.
    $processed_element = $this->decorated->process($original_element);

    if (!$processed_element) {
      // The element is empty.
      // Either there is no data to be displayed, or the decorated processor
      // returned an empty array because of '#access' => FALSE in the original
      // element.
      // Do not apply decorations to an empty element.
      return [];
    }

    // Check the '#access' property of the processed element.
    // We do not check '#access' on the original element. Instead, we assume
    // that whatever the decorated processor determined was by intention.
    if (isset($processed_element['#access']) && !$processed_element['#access']) {
      // Viewing access was denied.
      // This could either be a new '#access' property added by the decorated
      // processor, or the '#access' property from the original element.
      // Do not apply decorations to an inaccessible element.
      return [];
    }

    return $this->postProcess($processed_element, $original_element);
  }

  /**
   * Applies additional processing to the field render element.
   *
   * Typically this is to display additional elements or wrappers around the
   * main field display.
   *
   * @param array $processed_element
   *   Processed render element as returned from the decorated processor.
   *   This may no longer have the typical structure with '#theme' => 'field'.
   * @param array $original_element
   *   Original field render element with ['#theme' => 'field'].
   *
   * @return array
   *   Post-processed render element.
   *
   * @see theme_ds_field_minimal()
   * @see theme_field()
   */
  abstract protected function postProcess(array $processed_element, array $original_element);

}
