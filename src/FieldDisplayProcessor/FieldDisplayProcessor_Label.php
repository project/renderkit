<?php

namespace Drupal\renderkit\FieldDisplayProcessor;

/**
 * Decorator which prepends a label element.
 *
 * This is not registered as a plugin by itself.
 * It is only used inside other plugins via composition.
 */
class FieldDisplayProcessor_Label extends FieldDisplayProcessor_DecoratorBase {

  /**
   * @var string|null
   */
  private $labelUnsafe;

  /**
   * @var bool
   */
  private $appendLabelColon = TRUE;

  /**
   * @param string $labelUnsafe
   *
   * @return static
   */
  public function withCustomLabel($labelUnsafe) {
    $clone = clone $this;
    $clone->labelUnsafe = $labelUnsafe;
    return $clone;
  }

  /**
   * @return static
   */
  public function withoutLabelColon() {
    $clone = clone $this;
    $clone->appendLabelColon = FALSE;
    return $clone;
  }

  /**
   * {@inheritdoc}
   */
  protected function postProcess(array $modified, array $element) {

    if (!isset($element['#label_display']) || 'hidden' === $element['#label_display']) {
      return $modified;
    }

    if (NULL !== $this->labelUnsafe) {
      $label_safe = check_plain($this->labelUnsafe);
    }
    elseif (isset($element['#title']) && '' !== $element['#title']) {
      $label_safe = check_plain($element['#title']);
    }
    else {
      return $modified;
    }

    if ($this->appendLabelColon) {
      $label_safe .= ':&nbsp;';
    }

    return [
      'label' => [
        '#weight' => -100,
        '#type' => 'container',
        // Adds a class like 'label-inline' on the label itself, mimicking
        // theme_ds_field_minimal() from Display suite.
        // Note that this requires custom CSS to work properly:
        // https://www.drupal.org/project/ds/issues/1308000
        // Otherwise, in Display suite, "inline" field labels display above the
        // field when "full reset" or "minimal" is chosen.
        '#attributes' => ['class' => ['label-' . $element['#label_display']]],
        '#children' => $label_safe,
      ],
      'items' => $modified,
    ];
  }

}
