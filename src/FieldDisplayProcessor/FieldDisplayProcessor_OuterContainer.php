<?php

namespace Drupal\renderkit\FieldDisplayProcessor;

/**
 * Wrap the field display in an outer container element.
 */
class FieldDisplayProcessor_OuterContainer extends FieldDisplayProcessor_DecoratorBase {

  /**
   * @var string[]
   */
  private $classes = [];

  /**
   * @param string[] $classes
   *
   * @return static
   */
  public function withAdditionalClasses(array $classes) {
    $clone = clone $this;
    foreach ($classes as $class) {
      $clone->classes[] = $class;
    }
    return $clone;
  }

  /**
   * {@inheritdoc}
   */
  protected function postProcess(array $modified, array $element) {

    $classes = [
      'field',
      'field-name-' . str_replace('_', '-', $element['#field_name']),
    ];

    foreach ($this->classes as $class) {
      $classes[] = $class;
    }

    $modified = [
      '#type' => 'container',
      'inner' => $modified,
    ];

    if ([] !== $classes) {
      $modified['#attributes']['class'] = $classes;
    }

    return $modified;
  }

}
