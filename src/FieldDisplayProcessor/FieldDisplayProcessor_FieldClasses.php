<?php

namespace Drupal\renderkit\FieldDisplayProcessor;

/**
 * Decorator to add default field classes to the processed element.
 *
 * If the decorated component fully rewrites the field display render element,
 * this decorator can make it behave a little bit more like the original field
 * display.
 */
class FieldDisplayProcessor_FieldClasses extends FieldDisplayProcessor_DecoratorBase {

  /**
   * {@inheritdoc}
   */
  protected function postProcess(array $build, array $element) {
    $build['#attributes']['class'][] = 'field';
    $build['#attributes']['class'][] = 'field-name-' . str_replace('_', '-', $element['#field_name']);
    $build['#attributes']['class'][] = 'field-type-' . str_replace('_', '-', $element['#field_type']);
    $build['#attributes']['class'][] = 'field-label-' . $element['#label_display'];
    return $build;
  }

}
