<?php

namespace Drupal\renderkit\CfrGroupSchema;

use Drupal\cfrapi\CfrGroupSchema\CfrGroupSchema_DecoratorBase;
use Drupal\cfrapi\CfrGroupSchema\CfrGroupSchemaInterface;
use Drupal\renderkit\Configurator\Configurator_ClassAttribute;
use Drupal\renderkit\FieldDisplayProcessor\FieldDisplayProcessor_OuterContainer;
use Drupal\renderkit\FieldDisplayProcessor\FieldDisplayProcessorInterface;

class CfrGroupSchema_FieldDisplayProcessor_OuterContainer extends CfrGroupSchema_DecoratorBase {

  /**
   * @var bool
   */
  private $withClassesOption;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\CfrGroupSchema\CfrGroupSchemaInterface $decorated
   *   Decorated group schema.
   * @param bool $withClassesOption
   *   TRUE, to allow to add custom classes for the container.
   */
  public function __construct(CfrGroupSchemaInterface $decorated, $withClassesOption = TRUE) {
    parent::__construct($decorated);
    $this->withClassesOption = $withClassesOption;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurators() {
    $configurators = parent::getConfigurators();
    if ($this->withClassesOption) {
      $configurators['classes'] = new Configurator_ClassAttribute();
    }
    return $configurators;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabels() {
    $labels = parent::getLabels();
    if ($this->withClassesOption) {
      $labels['classes'] = t('Additional classes');
    }
    return $labels;
  }

  /**
   * {@inheritdoc}
   */
  public function valuesGetValue(array $values) {

    $fdp = parent::valuesGetValue($values);

    if (!$fdp instanceof FieldDisplayProcessorInterface) {
      return $fdp;
    }

    $fdp = new FieldDisplayProcessor_OuterContainer($fdp);

    if ($this->withClassesOption && [] !== $values['classes']) {
      $fdp = $fdp->withAdditionalClasses($values['classes']);
    }

    return $fdp;
  }

}
