<?php

namespace Drupal\renderkit;

/**
 * Class with helper methods to mark literals as callable or as Drupal hook.
 *
 * The "R" stands for "Renderkit".
 * Class and method names are kept very short, to minimize code clutter.
 *
 * Introduced in issue #3163161.
 */
class R {

  /**
   * Trick method to mark array or string literals as callable for an IDE.
   *
   * This adds a tiny bit of overhead and should not be used in everyday code,
   * but is ok for code that runs periodically or only once, e.g. hook_schema(),
   * hook_menu() etc,
   *
   * @param callable $callable
   *   The value to mark as callable.
   *
   * @return callable
   *   The same value as was passed in.
   */
  public static function c($callable) {
    if (!is_callable($callable)) {
      throw new \RuntimeException('Callback is not callable.');
    }
    return $callable;
  }

  /**
   * Trick method to mark array or string literals as callable for an IDE.
   *
   * Unlike c(), this one does not check whether the callable is valid.
   * This way it can be used for functions in files that are not included yet.
   *
   * This adds a tiny bit of overhead and should not be used in everyday code,
   * but is ok for code that runs periodically or only once, e.g. hook_schema(),
   * hook_menu() etc,
   *
   * @param callable $callable
   *   The value to mark as callable.
   *
   * @return callable
   *   The same value as was passed in.
   */
  public static function f($callable) {
    return $callable;
  }

  /**
   * Trick method to declare theme hooks in an IDE-friendly way.
   *
   * @param callable $theme_function
   *   Theme function with 'theme_' prefix.
   *   This allows an IDE to link directly to the theme function.
   *
   * @return string
   *   Theme hook name with the 'theme_' prefix removed.
   */
  public static function th($theme_function) {
    return substr($theme_function, 6);
  }

  /**
   * Trick method to call theme() with a full theme function name.
   *
   * @param callable $theme_function
   *   Theme function with 'theme_' prefix.
   *   This allows an IDE to link directly to the theme function.
   *
   * @return string
   *   The themed html.
   *
   * @throws \Exception
   *   On paper, theme() can throw exceptions.
   */
  public static function theme($theme_function) {
    return theme(substr($theme_function, 6));
  }

  /**
   * Trick method to mark hook_menu() wildcard fragments.
   *
   * @param callable $load_function
   *   Load function with '_load' suffix, e.g. 'node_load'.
   *   This allows an IDE to link directly to the loader function.
   *
   * @return string
   *   The wildcard fragment, e.g. '%node'.
   */
  public static function w($load_function) {
    return '%' . substr($load_function, 0, -5);
  }

  /**
   * Trick method to specify a hook name in an IDE-friendly way.
   *
   * @param callable $hook_function
   *   The complete hook function, e.g. 'hook_menu'.
   *   This allows an IDE to link directly to the hook definition.
   *
   * @return string
   *   The name of the hook, e.g. 'menu'.
   */
  public static function hook($hook_function) {
    return substr($hook_function, 5);
  }

  /**
   * Trick function to make a form builder function clickable.
   *
   * @param callable $form_id
   *   The form id. Usually this corresponds to a function name.
   *   This allows the IDE to link directly to the form builder function.
   * @param mixed[] ...$args
   *   Additional arguments to pass to the form builder.
   *
   * @return array
   *   Render element for the form.
   */
  public static function drupal_get_form($form_id, ...$args) {
    return \drupal_get_form($form_id, ...$args);
  }

}
