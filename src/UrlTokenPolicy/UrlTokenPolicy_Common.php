<?php

namespace Drupal\renderkit\UrlTokenPolicy;

/**
 * Common url token implementation.
 */
class UrlTokenPolicy_Common implements UrlTokenPolicyInterface {

  /**
   * Query key for the token itself.
   *
   * @var string
   */
  private $tokenKey;

  /**
   * Truthmap of query keys used when computing the token.
   *
   * @var true[]
   */
  private $keysMap;

  /**
   * Constructor.
   *
   * @param string $token_key
   *   The query key that contains the token.
   * @param string[] $query_keys_to_verify
   *   Query keys used when computing the token.
   */
  public function __construct($token_key, array $query_keys_to_verify) {
    $this->tokenKey = $token_key;
    $this->keysMap = ['q' => TRUE]
      + array_fill_keys($query_keys_to_verify, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function queryIsSigned(array $query) {
    if (!isset($query[$this->tokenKey])) {
      return FALSE;
    }
    $token_found = $query[$this->tokenKey];
    $token_calculated = $this->queryGetToken($query);
    return $token_found === $token_calculated;
  }

  /**
   * {@inheritdoc}
   */
  public function pathQuerySetToken($path, array &$query) {
    $query_copy = $query;
    $query_copy['q'] = $path;
    $token_calculated = $this->queryGetToken($query_copy);
    $query[$this->tokenKey] = $token_calculated;
  }

  /**
   * @param array $query
   *   The url query, including 'q' for the path.
   *
   * @return string
   *   Token.
   */
  private function queryGetToken(array $query) {

    // Reduce the query to only the relevant keys.
    $query = array_intersect_key($query, $this->keysMap);

    $datastring = drupal_http_build_query($query);

    if (isset($GLOBALS['user']->uid)) {
      $datastring = 'u/' . $GLOBALS['user']->uid . '?' . $datastring;
    }

    return substr(
      drupal_hmac_base64(
        $datastring,
        drupal_get_private_key() . drupal_get_hash_salt()),
      0,
      16);
  }

}
