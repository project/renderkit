<?php

namespace Drupal\renderkit\LabeledEntityBuildProcessor;

/**
 * Displays a labeled element, informed by an entity.
 *
 * @todo This interface does not seem very useful.
 */
interface LabeledEntityBuildProcessorInterface {

  /**
   * Displays a labeled element, informed by an entity.
   *
   * @param array $build
   *   Original content render element.
   * @param string $entityType
   *   Entity type.
   * @param object $entity
   *   Entity that should inform the display.
   * @param string $label
   *   Label.
   *
   * @return array
   *   Render element showing the content + label.
   */
  public function buildAddLabelWithEntity(array $build, $entityType, $entity, $label);

}
