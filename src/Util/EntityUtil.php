<?php

namespace Drupal\renderkit\Util;

final class EntityUtil extends UtilBase {

  /**
   * @param string $entityType
   * @param object $entity
   *
   * @return int|null
   *
   * @see \entity_id()
   * @see \entity_extract_ids()
   */
  public static function entityGetId($entityType, $entity) {

    $info = entity_get_info($entityType);
    $idKey = $info['entity keys']['id'];

    return isset($entity->$idKey) ? $entity->$idKey : NULL;
  }

  /**
   * @param string $entityType
   * @param object[] $entities
   *
   * @return int[]
   */
  public static function entitiesGetIds($entityType, $entities) {

    $info = entity_get_info($entityType);
    $idKey = $info['entity keys']['id'];

    $idsByDelta = [];
    foreach ($entities as $delta => $entity) {
      if (isset($entity->$idKey)) {
        $idsByDelta[$delta] = $entity->$idKey;
      }
    }
    return $idsByDelta;
  }

  /**
   * Checks access for a single entity.
   *
   * This avoids depending on 'entity' module.
   *
   * @param string $entityType
   * @param object $entity
   * @param string $op
   * @param object|null $account
   *
   * @return bool|null
   *
   * @see \entity_access()
   */
  public static function entityCheckAccess($entityType, $entity, $op = 'view', $account = NULL) {

    if (!$info = entity_get_info($entityType)) {
      // We cannot determine access.
      return NULL;
    }

    if (!isset($info['access callback'])) {
      // We cannot determine access.
      return NULL;
    }

    return $info['access callback']($op, $entity, $account, $entityType);
  }

  /**
   * Checks access for an array of entities.
   *
   * This has performance benefits over the single-entity method.
   *
   * @param string $entityType
   * @param object[] $entities
   *   Format: $[$delta] = $entity
   * @param string $op
   * @param object|null $account
   *
   * @return object[]
   *   Format: $[$delta] = $entity
   *
   * @see \entity_access()
   */
  public static function entitiesCheckAccess($entityType, array $entities, $op = 'view', $account = NULL) {

    if (!$info = entity_get_info($entityType)) {
      // Act as if none of the entities has access.
      return [];
    }

    if (!isset($info['access callback'])) {
      // Act as if none of the entities has access.
      return [];
    }

    $accessCallback = $info['access callback'];

    $entitiesWithAccess = [];
    foreach ($entities as $delta => $entity) {
      if ($accessCallback($op, $entity, $account, $entityType)) {
        $entitiesWithAccess[$delta] = $entity;
      }
    }

    return $entitiesWithAccess;
  }

  /**
   * Checks access for a nested array of entities.
   *
   * This helps to simplify calling code that deals with nested arrays.
   *
   * @param string $entityType
   * @param object[][] $entitiess
   *   Format: $[$delta0][$delta1] = $entity
   * @param string $op
   * @param object|null $account
   *
   * @return object[][]
   *   Format: $[$delta0][$delta1] = $entity
   *
   * @see \entity_access()
   */
  public static function entitiessCheckAccess($entityType, array $entitiess, $op = 'view', $account = NULL) {

    if (!$info = entity_get_info($entityType)) {
      // Act as if none of the entities has access.
      return [];
    }

    if (!isset($info['access callback'])) {
      // Act as if none of the entities has access.
      return [];
    }

    $accessCallback = $info['access callback'];

    $entitiessWithAccess = [];
    foreach ($entitiess as $delta0 => $entities) {
      foreach ($entities as $delta1 => $entity) {
        if ($accessCallback($op, $entity, $account, $entityType)) {
          $entitiessWithAccess[$delta0][$delta1] = $entity;
        }
      }
    }

    return $entitiessWithAccess;
  }

}
