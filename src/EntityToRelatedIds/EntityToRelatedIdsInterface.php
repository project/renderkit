<?php

namespace Drupal\renderkit\EntityToRelatedIds;

/**
 * Gets a list of related entity ids for a given entity.
 */
interface EntityToRelatedIdsInterface {

  /**
   * Gets the entity type for target entities.
   *
   * @return string
   *   The target entity type.
   */
  public function getTargetType();

  /**
   * Gets a list of related entity ids for a given entity.
   *
   * @param string $entityType
   *   The source entity type.
   * @param object $entity
   *   The source entity.
   *
   * @return int[]
   *   List of target entity ids for the source entity.
   *   Format: $[] = $relatedEntityId.
   *   There is no guarantee that the source entities actually exist.
   */
  public function entityGetRelatedIds($entityType, $entity);

  /**
   * Bulk version of the method above.
   *
   * Implementations must behave equivalent to the single-value version.
   *
   * @param string $entityType
   *   The source entity type.
   * @param object[] $entities
   *   The source entities.
   *   Format: $[$delta] = $entity.
   *
   * @return int[][]
   *   Lists of target entity ids for each source entity.
   *   Format: $[$delta][] = $relatedEntityId.
   *   There is no guarantee that the source entities actually exist.
   */
  public function entitiesGetRelatedIds($entityType, array $entities);

}
