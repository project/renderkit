<?php

namespace Drupal\renderkit\EntityDisplay\Decorator;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\renderkit\EntityDisplay\EntityDisplay;
use Drupal\renderkit\EntityDisplay\EntityDisplayInterface;
use Drupal\renderkit\EntityToEntity\EntityToEntity;
use Drupal\renderkit\EntityToEntity\EntityToEntityInterface;
use Drupal\renderkit\Util\EntityUtil;

class EntityDisplay_RelatedEntity implements EntityDisplayInterface {

  /**
   * @var \Drupal\renderkit\EntityToEntity\EntityToEntityInterface
   */
  private $entityToEntity;

  /**
   * @var \Drupal\renderkit\EntityDisplay\EntityDisplayInterface
   */
  private $relatedEntityDisplay;

  /**
   * @CfrPlugin(
   *   id = "related",
   *   label = "Related entity"
   * )
   *
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function createConfigurator(CfrContextInterface $context = NULL) {
    return Configurator_CallbackConfigurable::createFromClassName(
      __CLASS__,
      [
        EntityToEntity::configurator($context),
        // This one is without context, because we don't know the target entity type.
        EntityDisplay::configurator(),
      ],
      [
        t('Entity relation'),
        t('Entity display for target entity'),
      ]
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityToEntity\EntityToEntityInterface $entityToEntity
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface $relatedEntityDisplay
   */
  public function __construct(EntityToEntityInterface $entityToEntity, EntityDisplayInterface $relatedEntityDisplay) {
    $this->entityToEntity = $entityToEntity;
    $this->relatedEntityDisplay = $relatedEntityDisplay;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntities($entityType, array $entities) {

    $targetEntities = $this->entityToEntity->entitiesGetRelated(
      $entityType,
      $entities);

    if ([] === $targetEntities) {
      return [];
    }

    $targetEntities = EntityUtil::entitiesCheckAccess(
      $targetEntityType = $this->entityToEntity->getTargetType(),
      $targetEntities);

    if ([] === $targetEntities) {
      return [];
    }

    return $this->relatedEntityDisplay->buildEntities(
      $targetEntityType,
      $targetEntities);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity($entity_type, $entity) {

    if (NULL === $targetEntity = $this->entityToEntity->entityGetRelated($entity_type, $entity)) {
      return [];
    }

    if (!EntityUtil::entityCheckAccess(
      $targetEntityType = $this->entityToEntity->getTargetType(),
      $targetEntity)
    ) {
      return [];
    }

    return $this->relatedEntityDisplay->buildEntity($targetEntityType, $targetEntity);
  }

}
