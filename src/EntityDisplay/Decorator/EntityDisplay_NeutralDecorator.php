<?php

namespace Drupal\renderkit\EntityDisplay\Decorator;

use Drupal\renderkit\EntityDisplay\EntitiesDisplayBase;
use Drupal\renderkit\EntityDisplay\EntityDisplayInterface;

class EntityDisplay_NeutralDecorator extends EntitiesDisplayBase {

  /**
   * @var \Drupal\renderkit\EntityDisplay\EntityDisplayInterface|null
   */
  private $decorated;

  /**
   * Constructor.
   *
   * Sets the decorated entity display.
   *
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface $decorated
   */
  public function __construct(EntityDisplayInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntities($entityType, array $entities) {
    return isset($this->decorated)
      ? $this->decorated->buildEntities($entityType, $entities)
      : [];
  }

}
