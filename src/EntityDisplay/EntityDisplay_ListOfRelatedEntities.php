<?php

namespace Drupal\renderkit\EntityDisplay;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\renderkit\EntitiesListFormat\EntitiesListFormatInterface;
use Drupal\renderkit\EntityToEntities\EntityToEntitiesInterface;
use Drupal\renderkit\Util\EntityUtil;

class EntityDisplay_ListOfRelatedEntities implements EntityDisplayInterface {

  /**
   * @var \Drupal\renderkit\EntityToEntities\EntityToEntitiesInterface
   */
  private $entityToEntities;

  /**
   * @var \Drupal\renderkit\EntitiesListFormat\EntitiesListFormatInterface
   */
  private $entitiesListFormat;

  /**
   * @CfrPlugin("listOfRelatedEntities", "Related entities")
   *
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function plugin(CfrContextInterface $context = NULL) {
    return Configurator_CallbackConfigurable::createFromClassName(
      __CLASS__,
      [
        cfrplugin()->interfaceGetConfigurator(
          EntityToEntitiesInterface::class,
          $context),
        // This one is without context, because we don't know the target entity type.
        cfrplugin()->interfaceGetConfigurator(EntitiesListFormatInterface::class),
      ],
      [
        t('Entity to entities'),
        t('Entities list format for target entities'),
      ]
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityToEntities\EntityToEntitiesInterface $entityToEntities
   * @param \Drupal\renderkit\EntitiesListFormat\EntitiesListFormatInterface $entitiesListFormat
   */
  public function __construct(EntityToEntitiesInterface $entityToEntities, EntitiesListFormatInterface $entitiesListFormat) {
    $this->entityToEntities = $entityToEntities;
    $this->entitiesListFormat = $entitiesListFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntities($entityType, array $entities) {

    /**
     * @var object[][] $targetEntitiess
     *   Format: $[$sourceEntityDelta][$targetEntityDelta] = $targetEntity
     */
    $targetEntitiess = $this->entityToEntities->entitiesGetRelated($entityType, $entities);

    $targetEntitiess = EntityUtil::entitiessCheckAccess(
      $targetEntityType = $this->entityToEntities->getTargetEntityType(),
      $targetEntitiess);

    $builds = [];
    foreach ($targetEntitiess as $sourceEntityDelta => $targetEntities) {

      $builds[$sourceEntityDelta] = $this->entitiesListFormat->entitiesBuildList(
        $targetEntityType,
        $targetEntities);
    }

    return $builds;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity($entity_type, $entity) {

    $targetEntities = $this->entityToEntities->entityGetRelated($entity_type, $entity);

    if ([] === $targetEntities) {
      return [];
    }

    $targetEntities = EntityUtil::entitiesCheckAccess(
      $targetEntityType = $this->entityToEntities->getTargetEntityType(),
      $targetEntities);

    if ([] === $targetEntities) {
      return [];
    }

    return $this->entitiesListFormat->entitiesBuildList(
      $targetEntityType,
      $targetEntities);
  }

}
