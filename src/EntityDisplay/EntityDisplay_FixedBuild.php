<?php

namespace Drupal\renderkit\EntityDisplay;

class EntityDisplay_FixedBuild extends EntityDisplayBase {

  /**
   * @var array
   */
  private $fixedRenderArray;

  /**
   * Constructor.
   *
   * @param array $fixedRenderArray
   */
  public function __construct(array $fixedRenderArray) {
    $this->fixedRenderArray = $fixedRenderArray;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity($entity_type, $entity) {
    return $this->fixedRenderArray;
  }

}
