<?php

namespace Drupal\renderkit\EntityDisplay;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\renderkit\IdToConfigurator\IdToConfigurator_DsLayoutRegions;

/**
 * Shows the entity in a Display Suite layout.
 */
class EntityDisplay_DsLayout extends EntitiesDisplayBase {

  /**
   * @var string
   */
  private $layoutName;

  /**
   * @var \Drupal\renderkit\EntityDisplay\EntityDisplayInterface[]|null[]
   */
  private $regionDisplays;

  /**
   * @CfrPlugin("dsLayout", "Ds layout")
   *
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *   (optional) Context to restrict available options.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator for this plugin, or NULL if not available.
   */
  public static function plugin(CfrContextInterface $context = NULL) {
    $configurator = IdToConfigurator_DsLayoutRegions::createDrilldown(
      EntityDisplay::optionalConfigurator($context));
    if (!$configurator) {
      return NULL;
    }
    return $configurator->withValueConstructor(self::class);
  }

  /**
   * Constructor.
   *
   * @param string $layoutName
   *   Layout name.
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface[]|null[] $regionDisplays
   *   Display for each region.
   */
  public function __construct($layoutName, array $regionDisplays) {
    $this->layoutName = $layoutName;
    $this->regionDisplays = $regionDisplays;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntities($entityType, array $entities) {

    $elements = [];
    foreach ($this->regionDisplays as $regionName => $display) {
      if (!$display) {
        continue;
      }
      foreach ($display->buildEntities($entityType, $entities) as $delta => $build) {
        $elements[$delta][$regionName] = $build;
      }
    }

    $elements = array_intersect_key($elements, $entities);

    foreach ($elements as $delta => &$element) {
      $element += [
        '#entity' => $entities[$delta],
        '#entity_type' => $entityType,
        '#type' => 'renderkit_ds_layout',
        '#layout_name' => $this->layoutName,
      ];
    }

    if (!user_access('access contextual links')) {
      return $elements;
    }

    foreach ($elements as $delta => &$element) {
      $entity = $entities[$delta];
      $entity_uri = entity_uri($entityType, $entity);
      if (null === $entity_uri || !isset($entity_uri['path'])) {
        continue;
      }
      $entity_id = entity_id($entityType, $entity);
      if (empty($entity_id)) {
        continue;
      }
      if (FALSE === $pos = strpos($entity_uri['path'] . '/', '/' . $entity_id . '/')) {
        continue;
      }
      $base_path = substr($entity_uri['path'], 0, $pos);
      $element['#contextual_links'] = [$entityType => [$base_path, [$entity_id]]];
    }

    return $elements;
  }

}
