<?php

namespace Drupal\renderkit\EntityDisplay\Switcher;

use Drupal\renderkit\EntityDisplay\EntitiesDisplayBase;
use Drupal\renderkit\EntityDisplay\EntityDisplayInterface;
use Drupal\renderkit\EntityFilter\EntityFilterInterface;

/**
 * @CfrPlugin(
 *   id = "filterDisplaySwitcher",
 *   label = "Conditional entity display"
 * )
 */
class EntityDisplay_Conditional extends EntitiesDisplayBase {

  /**
   * @var \Drupal\renderkit\EntityFilter\EntityFilterInterface
   */
  private $entityFilter;

  /**
   * @var \Drupal\renderkit\EntityDisplay\EntityDisplayInterface
   */
  private $displayIfTrue;

  /**
   * @var \Drupal\renderkit\EntityDisplay\EntityDisplayInterface|null
   */
  private $displayIfFalse;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityFilter\EntityFilterInterface $entityFilter
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface $displayIfTrue
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface|null $displayIfFalse
   */
  public function __construct(EntityFilterInterface $entityFilter, EntityDisplayInterface $displayIfTrue, EntityDisplayInterface $displayIfFalse = NULL) {
    $this->entityFilter = $entityFilter;
    $this->displayIfTrue = $displayIfTrue;
    $this->displayIfFalse = $displayIfFalse;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntities($entityType, array $entities) {
    $deltas = $this->entityFilter->entitiesFilterDeltas($entityType, $entities);
    $lookup = array_fill_keys($deltas, TRUE);
    $entitiesWithQuality = [];
    $entitiesWithoutQuality = [];
    $builds = [];
    foreach ($entities as $delta => $entity) {
      if (!empty($lookup[$delta])) {
        $entitiesWithQuality[$delta] = $entity;
      }
      else {
        $entitiesWithoutQuality[$delta] = $entity;
      }
      $builds[$delta] = [];
    }
    if ($entitiesWithQuality) {
      foreach ($this->displayIfTrue->buildEntities($entityType, $entitiesWithQuality) as $delta => $build) {
        $builds[$delta] = $build;
      }
    }
    if ($entitiesWithoutQuality && $this->displayIfFalse) {
      foreach ($this->displayIfFalse->buildEntities($entityType, $entitiesWithoutQuality) as $delta => $build) {
        $builds[$delta] = $build;
      }
    }
    # \Drupal\krumong\dpm(get_defined_vars(), TRUE);
    return $builds;
  }

}
