<?php

namespace Drupal\renderkit\EntityDisplay;

use Drupal\renderkit\BuildProvider\BuildProviderInterface;

/**
 * @CfrPlugin("buildProvider", "Build provider (static element)")
 */
class EntityDisplay_BuildProvider extends EntityDisplayBase {

  /**
   * @var \Drupal\renderkit\BuildProvider\BuildProviderInterface
   */
  private $buildProvider;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\BuildProvider\BuildProviderInterface $buildProvider
   */
  public function __construct(BuildProviderInterface $buildProvider) {
    $this->buildProvider = $buildProvider;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity($entity_type, $entity) {
    return $this->buildProvider->build();
  }

}
