<?php

namespace Drupal\renderkit\EntityDisplay;

use Drupal\renderkit\EntityToEntity\EntityToEntityInterface;
use Drupal\renderkit\Util\EntityUtil;

/**
 * @deprecated
 * Use @see \Drupal\renderkit\EntityDisplay\Decorator\EntityDisplay_RelatedEntity instead.
 */
class EntityDisplay_Reference extends EntitiesDisplayBase {

  /**
   * @var \Drupal\renderkit\EntityDisplay\EntityDisplayInterface
   */
  private $decorated;

  /**
   * @var \Drupal\renderkit\EntityToEntity\EntityToEntityInterface
   */
  private $reference;

  /**
   * @param string $entityType
   *
   * @return array|bool
   */
  public static function author($entityType) {
    switch ($entityType) {
      case 'node':
        return [
          'propertyKey' => 'uid',
          'targetType' => 'user',
        ];
    }

    return FALSE;
  }

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface $decorated
   * @param \Drupal\renderkit\EntityToEntity\EntityToEntityInterface $reference
   */
  public function __construct(EntityDisplayInterface $decorated, EntityToEntityInterface $reference) {
    $this->decorated = $decorated;
    $this->reference = $reference;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntities($entityType, array $entities) {

    if ([] === $targetEntities = $this->reference->entitiesGetRelated(
      $entityType,
      $entities)
    ) {
      return [];
    }

    if ([] === $targetEntities = EntityUtil::entitiesCheckAccess(
        $targetEntityType = $this->reference->getTargetType(),
      $targetEntities)
    ) {
      return [];
    }

    return $this->decorated->buildEntities(
      $targetEntityType,
      $targetEntities);
  }

}
