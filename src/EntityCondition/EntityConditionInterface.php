<?php

namespace Drupal\renderkit\EntityCondition;

/**
 * Checks if an entity matches a condition.
 */
interface EntityConditionInterface {

  /**
   * Checks if an entity matches a condition.
   *
   * @param string $entityType
   *   The entity type.
   * @param object $entity
   *   The entity.
   *
   * @return bool
   *   TRUE, if the condition is true for the entity.
   */
  public function entityCheckCondition($entityType, $entity);

}
