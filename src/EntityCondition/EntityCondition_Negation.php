<?php

namespace Drupal\renderkit\EntityCondition;

/**
 * This is not annotated as CfrPlugin, because it is equivalent with
 *
 * @see \Drupal\renderkit\EntityFilter\EntityFilter_Negation
 */
class EntityCondition_Negation implements EntityConditionInterface {

  /**
   * @var \Drupal\renderkit\EntityCondition\EntityConditionInterface
   */
  private $negatedCondition;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityCondition\EntityConditionInterface $negatedCondition
   */
  public function __construct(EntityConditionInterface $negatedCondition) {
    $this->negatedCondition = $negatedCondition;
  }

  /**
   * {@inheritdoc}
   */
  public function entityCheckCondition($entityType, $entity) {
    return !$this->negatedCondition->entityCheckCondition($entityType, $entity);
  }

}
