<?php

namespace Drupal\renderkit\EntityCondition;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\renderkit\EntityToEntity\EntityToEntity;
use Drupal\renderkit\EntityToEntity\EntityToEntityInterface;

class EntityCondition_EntityToEntity implements EntityConditionInterface {

  /**
   * @var \Drupal\renderkit\EntityToEntity\EntityToEntityInterface
   */
  private $entityToEntity;

  /**
   * @var \Drupal\renderkit\EntityCondition\EntityConditionInterface
   */
  private $targetEntityCondition;

  /**
   * @CfrPlugin("related", "Related entity")
   *
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function plugin(CfrContextInterface $context = NULL) {
    return Configurator_CallbackConfigurable::createFromClassName(
      __CLASS__,
      [
        EntityToEntity::configurator($context),
        // This one is without context, because we don't know the target entity type.
        cfrplugin()->interfaceGetConfigurator(EntityConditionInterface::class),
      ],
      [
        t('Entity relation'),
        t('Entity condition for target entity'),
      ]
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityToEntity\EntityToEntityInterface $entityToEntity
   * @param \Drupal\renderkit\EntityCondition\EntityConditionInterface $relatedEntityCondition
   */
  public function __construct(EntityToEntityInterface $entityToEntity, EntityConditionInterface $relatedEntityCondition) {
    $this->entityToEntity = $entityToEntity;
    $this->targetEntityCondition = $relatedEntityCondition;
  }

  /**
   * {@inheritdoc}
   */
  public function entityCheckCondition($entityType, $entity) {

    if (NULL === $targetEntity = $this->entityToEntity->entityGetRelated($entityType, $entity)) {
      return FALSE;
    }

    return $this->targetEntityCondition->entityCheckCondition(
      $this->entityToEntity->getTargetType(),
      $targetEntity);
  }

}
