<?php

namespace Drupal\renderkit\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

class Configurator_ListSeparator implements ConfiguratorInterface {

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    return [
      '#title' => t('Separator'),
      /* @see theme_textfield() */
      '#type' => 'textfield',
      '#maxlength' => 20,
      '#size' => 6,
      '#default_value' => \is_string($conf) ? $conf : NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return \is_string($conf)
      ? check_plain(json_encode(substr($conf, 0, 7)))
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    if (!\is_string($conf)) {
      $conf = '';
    }
    return check_plain($conf);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    if (!\is_string($conf)) {
      $conf = '';
    }
    return var_export(check_plain($conf), TRUE);
  }

}
