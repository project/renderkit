<?php

namespace Drupal\renderkit\Configurator\Id;

use Drupal\cfrapi\Configurator\Id\Configurator_SelectBase;

/**
 * Configurator to choose a views display id.
 *
 * The resulting value is a string "$view_name:$display_id".
 *
 * @see \views_get_views_as_options()
 */
class Configurator_ViewsDisplayId extends Configurator_SelectBase {

  /**
   * {@inheritdoc}
   */
  protected function idIsKnown($id) {
    list($view_name, $display_id) = explode(':', $id . ':');
    return 1
      && '' !== $view_name
      && '' !== $display_id
      && NULL !== ($view = \views_get_view($view_name))
      && !empty($view->display[$display_id]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getSelectOptions() {
    $views = \views_get_all_views();

    $options = [];
    foreach ($views as $view_id => $view) {
      if (!empty($view->disabled)) {
        continue;
      }

      if (empty($view->display)) {
        // Skip this view as it is broken.
        continue;
      }

      $view_label = $view->get_human_name();

      foreach ($view->display as $display_id => $display) {

        if (!$this->displayIsSuitable($display)) {
          continue;
        }

        $options[$view_label][$view_id . ':' . $display_id] = $view_label
          . ' » ' . $this->displayGetLabel($display);
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function idGetLabel($id) {
    // Append ':' so that malformed values don't cause a notice.
    list($view_name, $display_id) = explode(':', $id . ':');
    if ('' === $view_name || '' === $display_id) {
      return NULL;
    }
    $view = \views_get_view($view_name);
    if (NULL === $view) {
      return NULL;
    }
    if (empty($view->display[$display_id])) {
      return NULL;
    }
    $display = $view->display[$display_id];
    return $view->get_human_name()
      . ': ' . $this->displayGetLabel($display);
  }

  /**
   * Gets a label for a views display, without the view name.
   *
   * @param \views_display $display
   *   Views display object.
   *
   * @return string
   *   Label to append to the view name.
   */
  private function displayGetLabel(\views_display $display) {
    $display_label = !empty($display->display_title)
      ? $display->display_title
      : $display->id;

    $plugins = views_fetch_plugin_data('display');

    $display_plugin_label = !empty($plugins[$display->display_plugin]['title'])
      ? $plugins[$display->display_plugin]['title']
      : $display->display_plugin;

    if (0 === strpos($display_label, $display_plugin_label)) {
      // Omit the display plugin name, as it would be redundant.
      return $display_label;
    }

    return $display_label . ' (' . $display_plugin_label . ')';
  }

  /**
   * Checks if a views display should be included in the options.
   *
   * This is protected, so it can be overridden in a subclass.
   *
   * @param \views_display $display
   *   Views display object.
   *
   * @return bool
   *   TRUE, to include this views display in the select options.
   */
  protected function displayIsSuitable(\views_display $display) {
    // By default, all views displays are suitable.
    return TRUE;
  }

}
