<?php

namespace Drupal\renderkit\Configurator\Id;

/**
 * Configurator to choose a views display id with entity argument.
 *
 * The value produced is a string of the format "$view_id:$display_id".
 * Only such views are available which take an entity id as parameter.
 */
class Configurator_ViewsDisplayId_Entity extends Configurator_ViewsDisplayId {

  /**
   * @var string|null
   */
  private $entityType;

  /**
   * Constructor.
   *
   * @param string|null $entityType
   *   (optional) Entity type to restrict the selection of views.
   * @param bool $required
   *   TRUE, if the selection of a views display is required.
   */
  public function __construct($entityType = NULL, $required = TRUE) {
    $this->entityType = $entityType;
    parent::__construct($required);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    $form = parent::confGetForm($conf, $label);
    // Add a description on the select element.
    $form['#description'] = t(
      'Only those views displays are shown where the argument input expects !type.',
      [
        '!type' => NULL !== $this->entityType
          ? t('entities of type @type', [
            '@type' => var_export($this->entityType, TRUE),
          ])
          : t('entities'),
      ]);
    $form['#description'] .= '<br/>'
      . t('Displays with more than one argument are not shown.');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function displayIsSuitable(\views_display $display) {

    if (empty($display->display_options['argument_input'])) {
      return FALSE;
    }

    if (\count($display->display_options['argument_input']) > 1) {
      return FALSE;
    }

    // Pick the first and only argument.
    $arg_options = reset($display->display_options['argument_input']);

    if (!isset($arg_options['context'])) {
      return FALSE;
    }

    $pattern = DRUPAL_PHP_FUNCTION_PATTERN;
    $regex = "@^entity:($pattern)\.($pattern)$@";

    if (!preg_match($regex, $arg_options['context'], $m)) {
      return FALSE;
    }

    /** @noinspection PhpUnusedLocalVariableInspection */
    list(, $entity_type, $id_key) = $m;

    if (NULL !== $this->entityType && $entity_type !== $this->entityType) {
      // Not the type we are looking for.
      return FALSE;
    }

    return TRUE;
  }

}
