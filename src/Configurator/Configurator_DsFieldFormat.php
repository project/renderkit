<?php

namespace Drupal\renderkit\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

class Configurator_DsFieldFormat implements ConfiguratorInterface {

  /**
   * @var array
   */
  private $field;

  /**
   * Constructor.
   *
   * @param array $info
   */
  public function __construct(array $info) {
    $this->field = $info;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    if (!\is_array($conf)) {
      $conf = [];
    }

    $form = [];

    if (isset($this->field['properties']['formatters'])) {
      $form['format'] = [
        '#type' => 'select',
        '#options' => $this->field['properties']['formatters'],
        '#default_value' => isset($conf['format'])
          ? $conf['format']
          : 'hidden',
        '#attributes' => ['class' => ['field-formatter-type']],
      ];
    }
    else {
      $form['format'] = [
        '#type' => 'value',
        '#value' => 'default',
      ];
    }

    $form['formatter_settings'] = module_invoke(
      $this->field['module'],
      'ds_field_settings_form',
      $this->field);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return $conf;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    return $helper->export($conf);
  }

}
