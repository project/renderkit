<?php

namespace Drupal\renderkit\Configurator;

use Drupal\cfrfamily\Configurator\Composite\Configurator_IdConfBase;
use Drupal\renderkit\Util\FieldUtil;

class Configurator_FieldNameWithFormatter extends Configurator_IdConfBase {

  /**
   * @var null|string
   */
  private $entityType;

  /**
   * @var null|string
   */
  private $bundleName;

  /**
   * Constructor.
   *
   * @param string $entityType
   * @param string $bundleName
   */
  public function __construct($entityType = NULL, $bundleName = NULL) {
    parent::__construct(TRUE, $this, 'field', 'display');
    $this->entityType = $entityType;
    $this->bundleName = $bundleName;
  }

  /**
   * {@inheritdoc}
   */
  protected function idGetConfigurator($fieldName) {

    $fieldInfo = field_info_field($fieldName);

    if (!isset($fieldInfo['type'])) {
      return NULL;
    }

    return new Configurator_FieldFormatter($fieldName, $fieldInfo['type']);
  }

  /**
   * {@inheritdoc}
   */
  protected function idGetOptionsFormLabel($id) {
    return t('Formatter');
  }

  /**
   * {@inheritdoc}
   */
  protected function getSelectOptions() {
    return FieldUtil::fieldTypesGetFieldNameOptions(NULL, $this->entityType, $this->bundleName);
  }

  /**
   * {@inheritdoc}
   */
  protected function idGetLabel($fieldName) {
    return FieldUtil::fieldnameEtBundleGetLabel($fieldName, $this->entityType, $this->bundleName);
  }

}
