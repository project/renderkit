<?php

namespace Drupal\renderkit\Configurator;

use Drupal\cfrapi\Configurator\Configurator_DecoratorBase;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;

class Configurator_Passthru extends Configurator_DecoratorBase {

  /**
   * Constructor. Public visibility, unlike the parent constructor.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $decorated
   */
  public function __construct(ConfiguratorInterface $decorated) {
    parent::__construct($decorated);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return $conf;
  }

}
