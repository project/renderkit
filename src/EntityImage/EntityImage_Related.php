<?php

namespace Drupal\renderkit\EntityImage;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\renderkit\EntityToEntity\EntityToEntity;
use Drupal\renderkit\EntityToEntity\EntityToEntityInterface;
use Drupal\renderkit\Util\EntityUtil;

class EntityImage_Related implements EntityImageInterface {

  /**
   * @var \Drupal\renderkit\EntityToEntity\EntityToEntityInterface
   */
  private $entityToEntity;

  /**
   * @var \Drupal\renderkit\EntityImage\EntityImageInterface
   */
  private $relatedEntityImage;

  /**
   * @CfrPlugin(
   *   id = "related",
   *   label = "Image from related entity"
   * )
   *
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function createConfigurator(CfrContextInterface $context = NULL) {
    return Configurator_CallbackConfigurable::createFromClassName(
      __CLASS__,
      [
        EntityToEntity::configurator($context),
        // This one is without context, because we no longer know the entity type.
        EntityImage::configurator(),
      ],
      [
        t('Entity relation'),
        t('Related entity image provider'),
      ]
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityToEntity\EntityToEntityInterface $entityToEntity
   * @param \Drupal\renderkit\EntityImage\EntityImageInterface $relatedEntityImage
   */
  public function __construct(EntityToEntityInterface $entityToEntity, EntityImageInterface $relatedEntityImage) {
    $this->entityToEntity = $entityToEntity;
    $this->relatedEntityImage = $relatedEntityImage;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity($entity_type, $entity) {

    if (NULL === $targetEntity = $this->entityToEntity->entityGetRelated($entity_type, $entity)) {
      return [];
    }

    if (!EntityUtil::entityCheckAccess(
      $targetEntityType = $this->entityToEntity->getTargetType(),
      $targetEntity)
    ) {
      return [];
    }

    return $this->relatedEntityImage->buildEntity(
      $targetEntityType,
      $targetEntity);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntities($entityType, array $entities) {

    if ([] === $targetEntities = $this->entityToEntity->entitiesGetRelated(
        $entityType,
        $entities)
    ) {
      return [];
    }

    if ([] === $targetEntities = EntityUtil::entitiesCheckAccess(
        $targetEntityType = $this->entityToEntity->getTargetType(),
        $targetEntities)
    ) {
      return [];
    }

    return $this->relatedEntityImage->buildEntities(
      $targetEntityType,
      $targetEntities);
  }

}
