<?php

namespace Drupal\renderkit\EntityImage;

class EntityImage_EntityLabelAsAltFallback implements EntityImageInterface {

  /**
   * @var \Drupal\renderkit\EntityImage\EntityImageInterface
   */
  private $decorated;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityImage\EntityImageInterface $decorated
   */
  public function __construct(EntityImageInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity($entity_type, $entity) {

    $image = $this->decorated->buildEntity($entity_type, $entity);

    if (empty($image)) {
      return [];
    }

    if (empty($image['alt'])) {
      $image['alt'] = entity_label($entity_type, $entity);
    }

    return $image;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntities($entityType, array $entities) {

    $label = NULL;

    $images = [];
    foreach ($this->decorated->buildEntities($entityType, $entities) as $delta => $image) {

      if (empty($image) || empty($entities[$delta])) {
        continue;
      }

      if (empty($image['alt'])) {
        $image['alt'] = NULL !== $label
          ? $label
          : $label = entity_label($entityType, $entities[$delta]);
      }
    }

    return $images;
  }

}
