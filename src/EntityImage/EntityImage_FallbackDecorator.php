<?php

namespace Drupal\renderkit\EntityImage;

/**
 * @CfrPlugin(
 *   id = "fallback",
 *   label = @t("Fallback")
 * )
 */
class EntityImage_FallbackDecorator implements EntityImageInterface {

  /**
   * @var \Drupal\renderkit\EntityImage\EntityImageInterface
   */
  private $decorated;

  /**
   * @var \Drupal\renderkit\EntityImage\EntityImageInterface
   */
  private $fallback;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityImage\EntityImageInterface $decorated
   * @param \Drupal\renderkit\EntityImage\EntityImageInterface $fallback
   */
  public function __construct(EntityImageInterface $decorated, EntityImageInterface $fallback) {
    $this->decorated = $decorated;
    $this->fallback = $fallback;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity($entity_type, $entity) {
    $build = $this->decorated->buildEntity($entity_type, $entity);
    if (\is_array($build) && [] !== $build) {
      return $build;
    }
    return $this->fallback->buildEntity($entity_type, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntities($entityType, array $entities) {
    $builds = array_fill_keys(array_keys($entities), NULL);
    $builds += $this->decorated->buildEntities($entityType, $entities);
    foreach (array_filter($this->decorated->buildEntities($entityType, $entities)) as $delta => $build) {
      unset($entities[$delta]);
      $builds[$delta] = $build;
    }
    foreach (array_filter($this->fallback->buildEntities($entityType, $entities)) as $delta => $build) {
      $builds[$delta] = $build;
    }
    return array_filter($builds);
  }

}
