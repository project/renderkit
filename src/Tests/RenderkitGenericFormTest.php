<?php

namespace Drupal\renderkit\Tests;

use Drupal\renderkit\R;

/**
 * Test for the 'renderkit_generic_form' element type.
 */
class RenderkitGenericFormTest extends RenderkitRenderTestBase {

  public static function getInfo() {
    // Note: getInfo() strings should not be translated.
    return [
      'name' => 'Renderkit generic form test',
      'description' => 'Tests the \'renderkit_generic_form\' element type.',
      'group' => 'Renderkit',
    ];
  }

  public function testGenericForm() {

    $form = [];
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => 'Text',
    ];
    $form['color'] = [
      '#type' => 'select',
      '#options' => [
        'red' => 'Red',
        'green' => 'Green',
      ]
    ];
    $form = system_settings_form($form);

    $form_id = R::f('_renderkit_generic_form');

    $element = drupal_get_form($form_id, $form);

    $html_expected_raw = <<<EOT
<form action="@action" method="post" id="@form_id_attribute" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-text">
  <label for="edit-text">Text </label>
 <input type="text" id="edit-text" name="text" value="" size="60" maxlength="128" class="form-text" />
</div>
<div class="form-item form-type-select form-item-color">
 <select id="edit-color" name="color" class="form-select"><option value="red">Red</option><option value="green">Green</option></select>
</div>
<input type="hidden" name="form_build_id" value="@form_build_id" />
<input type="hidden" name="form_token" value="@form_token" />
<input type="hidden" name="form_id" value="@form_id" />
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Save configuration" class="form-submit" /></div></div></form>
EOT;

    $html_replacements = [
      '@action' => $element['#action'],
      '@form_id_attribute' => str_replace('_', '-', $form_id),
      '@form_id' => $form_id,
      '@form_token' => $element['form_token']['#value'],
      '@form_build_id' => $element['form_build_id']['#value'],
    ];

    $html_expected = format_string(
      $html_expected_raw,
      $html_replacements);

    $this->assertDrupalRender($html_expected, $element);

    // Do it again, using the other mechanism.
    $element = $form;
    $element['#type'] = 'renderkit_generic_form';
    $element['#form_id_suffix'] = __METHOD__;

    // Prevent '--2' appended to ids.
    drupal_static_reset('drupal_html_id');

    $this->assertDrupalRender(
      $html_expected_raw,
      $element,
      array_fill_keys(
        array_keys($html_replacements),
        '[^"]+'));
  }

}
