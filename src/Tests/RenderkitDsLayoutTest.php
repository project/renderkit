<?php

namespace Drupal\renderkit\Tests;

/**
 * Test for the 'renderkit_generic_form' theme hook and element type.
 */
class RenderkitDsLayoutTest extends RenderkitRenderTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(array $modules = []) {
    $modules[] = 'ds';
    $modules[] = 'panels';
    parent::setUp($modules);
  }

  /**
   * Gets info about this test.
   */
  public static function getInfo() {
    // Note: getInfo() strings should not be translated.
    return [
      'name' => 'Renderkit ds layout test',
      'description' => 'Tests the \'renderkit_ds_layout\' theme hook and element type.',
      'group' => 'Renderkit',
    ];
  }

  /**
   * Tests the 'renderkit_ds_layout' theme hook.
   *
   * Multiple calls share a single method, to skip repeated site install.
   */
  public function testDsLayoutThemeHook() {
    // Basic ds layout, using '#theme'.
    $element = [
      '#theme' => 'renderkit_ds_layout',
      '#layout_name' => 'ds_3col',
      '#rendered_regions' => [
        'left' => 'Left',
        'middle' => '',
        'right' => 'Right',
      ],
    ];

    $expected = '
<div class="ds-3col clearfix">
  <div class="group-left">
    Left
  </div>
  <div class="group-middle">
  </div>
  <div class="group-right">
    Right
  </div>
</div>
';

    $subpatterns = [
      "\n    " => '\s*',
      "\n  " => '\s*',
      "\n" => '\s*',
      ' ' => ' +',
    ];

    $this->assertDrupalRender($expected, $element, $subpatterns);

    // Add settings to the layout.
    $element['#layout_settings']['layout_wrapper'] = 'article';
    $element['#layout_settings']['classes']['layout_class'][] = 'test-layout-class';
    $element['#layout_settings']['wrappers']['left'] = 'section';
    $element['#layout_settings']['classes']['left'][] = 'test-left-class';

    $expected = '
<article class="ds-3col test-layout-class clearfix">
  <section class="group-left test-left-class">
    Left
  </section>
  <div class="group-middle">
  </div>
  <div class="group-right">
    Right
  </div>
</article>
';

    $this->assertDrupalRender($expected, $element, $subpatterns);

    // Test the fallback for non-existing layouts.
    $element['#layout_name'] = 'non_existing_layout';

    $expected = '
<article class="renderkit-ds-layout renderkit-ds-layout--non-existing-layout test-layout-class">
  <section class="renderkit-ds-layout-region--left test-left-class">
    Left
  </section>
  <div class="renderkit-ds-layout-region--middle">
  </div>
  <div class="renderkit-ds-layout-region--right">
    Right
  </div>
</article>
';

    $this->assertDrupalRender($expected, $element, $subpatterns);
  }

  /**
   * Tests the 'renderkit_ds_layout' element type.
   *
   * Multiple calls share a single method, to skip repeated site install.
   */
  public function testDsLayoutElementType() {

    // Basic ds layout, using '#type'.
    $element = [
      '#type' => 'renderkit_ds_layout',
      '#layout_name' => 'ds_3col',
      'left' => ['#markup' => 'Left'],
      'middle' => ['#markup' => 'Middle'],
    ];

    $expected = '
<div class="ds-3col  clearfix">
  <div class="group-left">
    Left
  </div>
  <div class="group-middle">
    Middle
  </div>
  <div class="group-right">
  </div>
</div>
';

    $subpatterns = [
      "\n    " => '\s*',
      "\n  " => '\s*',
      "\n" => '\s*',
    ];

    $this->assertDrupalRender($expected, $element, $subpatterns);

    // Add settings to the layout.
    $element['#layout_settings']['layout_wrapper'] = 'article';
    $element['#layout_settings']['classes']['layout_class'][] = 'test-layout-class';
    $element['#layout_settings']['wrappers']['left'] = 'section';
    $element['#layout_settings']['classes']['left'][] = 'test-left-class';

    $expected = '
<article class="ds-3col test-layout-class clearfix">
  <section class="group-left test-left-class">
    Left
  </section>
  <div class="group-middle">
    Middle
  </div>
  <div class="group-right">
  </div>
</article>
';

    // Test non-existing layout name.
    $element['#layout_name'] = 'non_existing_layout';

    // The element type does not use a fallback template.
    $expected = 'LeftMiddle';

    $this->assertDrupalRender($expected, $element, $subpatterns);

    // Panels layout.
    $element = [
      '#type' => 'renderkit_ds_layout',
      '#layout_name' => 'panels-twocol',
      'left' => ['#markup' => 'Left'],
      'right' => ['#markup' => 'Right'],
    ];

    $expected = '
<div class="panel-display panel-2col clearfix">
  <div class="panel-panel panel-col-first">
    <div class="inside">
      Left
    </div>
  </div>
  <div class="panel-panel panel-col-last">
    <div class="inside">
      Right
    </div>
  </div>
</div>
';

    $subpatterns = [
      "\n      " => '\s*',
      "\n    " => '\s*',
      "\n  " => '\s*',
      "\n" => '\s*',
      '">' => '" *>',
    ];

    $this->assertDrupalRender($expected, $element, $subpatterns);
  }

}
