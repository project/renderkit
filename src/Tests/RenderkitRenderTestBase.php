<?php

namespace Drupal\renderkit\Tests;

/**
 * Base class for web tests in themekit.
 */
abstract class RenderkitRenderTestBase extends \DrupalWebTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp(array $modules = []) {
    $modules[] = 'renderkit';
    parent::setUp($modules);
  }

  /**
   * Builds a form, without the need for a dedicated form builder callback.
   *
   * @param array $form
   *   The form array.
   *
   * @return array|mixed
   *   Processed form array.
   */
  protected function buildForm(array $form) {
    $form_id = '?';
    $form_state = form_state_defaults() + ['values' => []];
    drupal_prepare_form($form_id, $form, $form_state);

    // Clear out all group associations as these might be different when
    // re-rendering the form.
    $form_state['groups'] = [];

    // Return a fully built form that is ready for rendering.
    return form_builder($form_id, $form, $form_state);
  }

  /**
   * Asserts that a render element produces expected output.
   *
   * @param string $expected
   *   Expected html returned from theme().
   * @param string $hook
   *   Theme hook to pass to theme().
   * @param array $variables
   *   Variables to pass to theme().
   *
   * @return bool
   *   TRUE on pass, FALSE on fail.
   *
   * @see assertThemeOutput()
   *   This core function is not as good as this one, I claim.
   */
  protected function assertTheme($expected, $hook, array $variables) {

    $message = ''
      . '<pre>theme(@hook, @variables)</pre>'
      . '';

    $replacements = [
      '@expected' => var_export($expected, TRUE),
      '@hook' => var_export($hook, TRUE),
      '@variables' => var_export($variables, TRUE),
    ];

    try {
      $actual = theme($hook, $variables);

      $replacements['@actual'] = var_export($actual, TRUE);

      if ($actual !== $expected) {
        $success = FALSE;
        $message .= ''
          . '<hr/>'
          . 'Output: <pre>@actual</pre>'
          . '<hr/>'
          . 'Expected: <pre>@expected</pre>'
          . '';
      }
      else {
        $success = TRUE;
        $message .= ''
          . '<hr/>'
          . 'Output: <pre>@expected</pre>'
          . '';
      }
    }
    catch (\Exception $e) {
      $success = FALSE;
      $replacements['@exception'] = _drupal_render_exception_safe($e);
      $message .= ''
        . '<hr/>'
        . 'Exception: @exception'
        . '<hr/>'
        . 'Expected: <pre>@expected</pre>'
        . '';
    }

    return $this->assert(
      $success,
      format_string($message, $replacements));
  }

  /**
   * Asserts that a render element produces expected output.
   *
   * @param string $expected
   *   Expected html to be returned from drupal_render().
   * @param array $element
   *   Render element to pass to drupal_render().
   * @param string[]|null $subpatterns
   *   (optional) Replacements with regular expressions.
   * @param string $delimiter
   *   (optional) Regular expression delimiter.
   *
   * @return bool
   *   TRUE on pass, FALSE on fail.
   */
  protected function assertDrupalRender($expected, array $element, array $subpatterns = NULL, $delimiter = '~') {

    $message = '<pre>drupal_render(@element)</pre>';
    $replacements['@element'] = var_export($element, TRUE);

    try {
      $actual = drupal_render($element);
    }
    catch (\Exception $e) {
      $replacements += _drupal_decode_exception($e);
      $message .= ''
        . '<h2>Exception</h2>'
        . '<code>%type</code> in line %line of <code>%file</code>:<br/>'
        . 'in <code>%function</code><br/>'
        . '<pre>!message</pre>'
        . '<h4>Backtrace</h4>'
        . '<pre>%trace</pre>'
        . '';

      return $this->fail(
        format_string($message, $replacements));
    }

    $this->pass(
      format_string($message, $replacements));

    return $this->assertHtml(
      $expected,
      $actual,
      $subpatterns,
      $delimiter);
  }

  /**
   * Asserts that HTML is as expected.
   *
   * @param string $expected
   *   Expected HTML, with optional placeholders for regular expressions.
   * @param string $actual
   *   Actual HTML to compare with.
   * @param string[]|null $subpatterns
   *   Regular expression fragments to insert for the placeholders.
   *   Or NULL to do a regular string comparison instead of regex matching.
   * @param string $delimiter
   *   (optional) Regular expression delimiter.
   *
   * @return bool
   *   TRUE on pass, FALSE on fail.
   */
  protected function assertHtml($expected, $actual, array $subpatterns = NULL, $delimiter = '~') {

    if (!is_string($actual)) {
      $replacements['@actual'] = var_export($actual, TRUE);
      return $this->fail(
        'Expected a string of HTML, found @actual instead.',
        $replacements);
    }

    $replacements['@actual'] = var_export($actual, TRUE);
    $replacements['@expected'] = var_export($expected, TRUE);

    if ($subpatterns === NULL) {
      // Regular string comparison.
      if ($expected !== $actual) {
        $message = <<<'EOT'
<div>Expected: <pre>@expected</pre></div>
<div>Output: <pre>@actual</pre></div>
EOT;
        return $this->fail(
          format_string($message, $replacements));
      }
    }
    else {
      // Regular expression check.
      $pattern_replacements = [];
      foreach ($subpatterns as $wildcard_key => $wildcard_replacement) {
        $pattern_replace_key = preg_quote($wildcard_key, $delimiter);
        $pattern_replacements[$pattern_replace_key] = $wildcard_replacement;
      }
      $pattern_body = preg_quote($expected, $delimiter);
      $pattern_body = strtr($pattern_body, $pattern_replacements);
      $pattern = $delimiter . '^' . $pattern_body . '$' . $delimiter;
      if (!preg_match($pattern, $actual)) {
        $message = <<<'EOT'
<div>Expected: <pre>@expected</pre></div>
<div>Subpatterns: <pre>@subpatterns</pre></div>
<div>Expected pattern: <pre>@pattern</pre></div>
<div>Output: <pre>@actual</pre></div>
EOT;
        $replacements['@subpatterns'] = var_export($subpatterns, TRUE);
        $replacements['@pattern'] = var_export($pattern, TRUE);
        return $this->fail(
          format_string($message, $replacements));
      }
    }
    $message = 'Output as expected: <pre>@actual</pre>';

    return $this->pass(
      format_string($message, $replacements));
  }

}
