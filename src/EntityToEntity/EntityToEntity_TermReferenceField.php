<?php

namespace Drupal\renderkit\EntityToEntity;

use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrreflection\Configurator\Configurator_CallbackMono;
use Drupal\renderkit\Configurator\Id\Configurator_FieldName;

class EntityToEntity_TermReferenceField extends EntityToEntityMultipleBase {

  /**
   * @var string
   */
  private $fieldName;

  /**
   * @CfrPlugin("termReferenceField", "Term reference field")
   *
   * @param string $entityType
   *   (optional) Contextual parameter.
   * @param string $bundleName
   *   (optional) Contextual parameter.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function createConfigurator($entityType = NULL, $bundleName = NULL) {

    return Configurator_CallbackMono::createFromClassStaticMethod(
      self::class,
      'create',
      new Configurator_FieldName(
        ['taxonomy_term_reference'],
        $entityType,
        $bundleName));
  }

  /**
   * @param string $fieldName
   *
   * @return self|null
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  public static function create($fieldName) {
    $fieldInfo = field_info_field($fieldName);

    if (NULL === $fieldInfo) {
      throw new ConfToValueException("Field '$fieldName' does not exist.");
    }

    if (!isset($fieldInfo['type'])) {
      throw new ConfToValueException("Field '$fieldName' has no field type.");
    }

    if ($fieldInfo['type'] !== 'taxonomy_term_reference') {
      $typeExport = var_export($fieldInfo['type'], TRUE);
      throw new ConfToValueException("Field type of '$fieldName' expected to be 'taxonomy_term_reference', $typeExport found instead.");
    }

    return new self($fieldName);
  }

  /**
   * Constructor.
   *
   * @param string $fieldName
   */
  public function __construct($fieldName) {
    $this->fieldName = $fieldName;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetType() {
    return 'taxonomy_term';
  }

  /**
   * {@inheritdoc}
   */
  public function entitiesGetRelated($entityType, array $entities) {

    $tids_by_delta = [];
    foreach ($entities as $delta => $entity) {
      $items = field_get_items($entityType, $entity, $this->fieldName) ?: NULL;
      if (NULL === $items) {
        continue;
      }
      $item = reset($items);
      if (empty($item['tid'])) {
        continue;
      }
      $tids_by_delta[$delta] = $item['tid'];
    }

    $terms_by_tid = entity_load('taxonomy_term', $tids_by_delta);

    $terms_by_delta = [];
    foreach ($tids_by_delta as $delta => $tid) {
      if (array_key_exists($tid, $terms_by_tid)) {
        $terms_by_delta[$delta] = $terms_by_tid[$tid];
      }
    }

    return $terms_by_delta;
  }

}
