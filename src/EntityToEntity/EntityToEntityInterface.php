<?php

namespace Drupal\renderkit\EntityToEntity;

/**
 * Represents a one-to-one relation between entities.
 */
interface EntityToEntityInterface {

  /**
   * Gets the entity type of the referenced entities.
   *
   * @return string
   *   Entity type of target entities.
   */
  public function getTargetType();

  /**
   * Gets related entities for multiple source entities at once.
   *
   * The implementation MUST be equivalent with the single-value method.
   * The multiple-value version only exists for performance considerations.
   *
   * @param string $entityType
   *   The source entity type.
   * @param object[] $entities
   *   List of source entities.
   *   Format: $[$sourceEntityDelta] = $sourceEntity.
   *
   * @return object[]
   *   List of related entities, using the same array keys as $entities.
   *   Format: $[$sourceEntityDelta] = $targetEntity.
   */
  public function entitiesGetRelated($entityType, array $entities);

  /**
   * Gets a related entity from an entity.
   *
   * @param string $entityType
   *   The source entity type.
   * @param object $entity
   *   THe source entity.
   *
   * @return object|null
   *   The target entity, or NULL if none found.
   */
  public function entityGetRelated($entityType, $entity);

}
