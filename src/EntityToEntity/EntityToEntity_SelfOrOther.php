<?php

namespace Drupal\renderkit\EntityToEntity;

class EntityToEntity_SelfOrOther implements EntityToEntityInterface {

  /**
   * @var \Drupal\renderkit\EntityToEntity\EntityToEntityInterface
   */
  private $decorated;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityToEntity\EntityToEntityInterface $decorated
   */
  public function __construct(EntityToEntityInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetType() {
    return $this->decorated->getTargetType();
  }

  /**
   * {@inheritdoc}
   */
  public function entitiesGetRelated($entityType, array $entities) {
    if ($entityType === $this->decorated->getTargetType()) {
      return $entities;
    }

    return $this->decorated->entitiesGetRelated($entityType, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function entityGetRelated($entityType, $entity) {
    if ($entityType === $this->getTargetType()) {
      return $entity;
    }

    return $this->decorated->entityGetRelated($entityType, $entity);
  }

}
