<?php

namespace Drupal\renderkit\Html;

/**
 * Represents a list of HTML attributes.
 *
 * For now this is not very powerful.
 */
interface HtmlAttributesInterface {

  /**
   * Fluent setter. Adds a class name for the class attribute.
   *
   * @param string $class
   *   Class to add.
   *
   * @return $this
   */
  public function addClass($class);

  /**
   * Fluent setter. Adds class names for the class attribute.
   *
   * @param string[] $classes
   *   Class names to add.
   *
   * @return $this
   */
  public function addClasses(array $classes);

}
