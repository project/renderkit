<?php

namespace Drupal\renderkit\Html;

/**
 * Trait for classes with a settable tag name and attributes.
 *
 * @see \Drupal\renderkit\Html\HtmlTagInterface
 */
trait HtmlTagTrait {

  use HtmlAttributesTrait;

  /**
   * @var string
   */
  private $tagName = 'div';

  /**
   * @param string $tagName
   *
   * @return $this
   */
  public function setTagName($tagName) {
    $this->tagName = $tagName;
    return $this;
  }

  /**
   * Builds a 'themekit_container' render element based on current values.
   *
   * @return array
   */
  protected function buildContainer() {
    return $this->tagNameBuildContainer($this->tagName);
  }

}
