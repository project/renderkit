<?php

namespace Drupal\renderkit\Html;

/**
 * Interface for classes with a settable tag name.
 *
 * Notes:
 * - Setter interfaces are of limited usefulness, because normally the setters
 *   will be called at a time when the real class name is still known.
 * - Ideally, we should provide immutable setters (withers). However, for BC
 *   reasons, we cannot add those withers to an existing interface.
 *
 * @see \Drupal\renderkit\Html\HtmlTagTrait
 */
interface HtmlTagInterface extends HtmlAttributesInterface {

  /**
   * Fluent setter. Sets the tag name.
   *
   * @param string $tagName
   *   Tag name to set.
   *
   * @return $this
   */
  public function setTagName($tagName);

}
