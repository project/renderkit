<?php

namespace Drupal\renderkit\IdToConfigurator;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\Configurator\Group\Configurator_Group;
use Drupal\cfrfamily\Configurator\Composite\Configurator_IdConf;
use Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface;
use Drupal\renderkit\Legend\Legend_DsLayout;

/**
 * Object to build a sub-configurator from a ds layout name.
 */
class IdToConfigurator_DsLayoutRegions implements IdToConfiguratorInterface {

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private $regionConfigurator;

  /**
   * Creates a configurator for a ds layout + regions.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $regionConfigurator
   *   Configurator for region content.
   *
   * @return \Drupal\cfrfamily\Configurator\Composite\Configurator_IdConf|null
   *   Created configurator, or NULL if 'ds' not installed.
   */
  public static function createDrilldown(ConfiguratorInterface $regionConfigurator) {
    // Checking function_exists() is faster than module_exists().
    if (!function_exists('ds_get_layout_info')) {
      // Display Suite is not installed.
      // Return early, to prevent exceptions in the code below.
      return NULL;
    }
    try {
      $configurator = new Configurator_IdConf(
        new Legend_DsLayout(),
        new self($regionConfigurator));
    }
    catch (\Exception $e) {
      return NULL;
    }
    return $configurator->withIdLabel(t('Layout'));
  }

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $regionConfigurator
   *   Configurator for region content.
   *
   * @throws \Exception
   *   If module 'ds' does not exist.
   */
  public function __construct(ConfiguratorInterface $regionConfigurator) {
    // Checking function_exists() is faster than module_exists().
    if (!function_exists('ds_get_layout_info')) {
      throw new \Exception('Display Suite is not installed.');
    }
    $this->regionConfigurator = $regionConfigurator;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetConfigurator($layout_name) {
    $layouts = ds_get_layout_info();
    if (!isset($layouts[$layout_name])) {
      return NULL;
    }
    $layout = $layouts[$layout_name];
    if (empty($layout['regions'])) {
      // Layout without regions?
      return NULL;
    }
    $regions = $layout['regions'];
    unset($regions['ds_hidden']);
    if (!$regions) {
      // Layout without active regions?
      return NULL;
    }
    $group_configurator = new Configurator_Group();
    foreach ($regions as $region_name => $region_label) {
      $group_configurator->keySetConfigurator(
        $region_name,
        $this->regionConfigurator,
        t('Layout region: %region', ['%region' => $region_label]));
    }
    return $group_configurator;
  }

}
