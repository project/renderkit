<?php

namespace Drupal\renderkit\LabeledListFormat;

/**
 * Formats a list of elements with a label.
 */
interface LabeledListFormatInterface {

  /**
   * Formats a list of elements with a label.
   *
   * @param array[] $builds
   *   Render elements for list items.
   * @param string $label
   *   A label for the entire list.
   *
   * @return array
   *   Combined render element.
   */
  public function build(array $builds, $label);

}
