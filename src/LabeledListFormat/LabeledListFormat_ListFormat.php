<?php

namespace Drupal\renderkit\LabeledListFormat;

use Drupal\renderkit\ListFormat\ListFormatInterface;

/**
 * @CfrPlugin("listformat", "List format", inline = true)
 */
class LabeledListFormat_ListFormat implements LabeledListFormatInterface {

  /**
   * @var \Drupal\renderkit\ListFormat\ListFormatInterface
   */
  private $listFormat;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\ListFormat\ListFormatInterface $listFormat
   */
  public function __construct(ListFormatInterface $listFormat) {
    $this->listFormat = $listFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $builds, $label) {
    return $this->listFormat->buildList($builds);
  }

}
