<?php

namespace Drupal\renderkit\EntityToEntities;

abstract class EntityToEntitiesMultipleBase implements EntityToEntitiesInterface {

  /**
   * {@inheritdoc}
   */
  public function entityGetRelated($entityType, $entity) {
    $targetEntities = $this->entitiesGetRelated($entityType, [$entity]);
    return isset($targetEntities[0]) ? $targetEntities[0] : [];
  }

}
