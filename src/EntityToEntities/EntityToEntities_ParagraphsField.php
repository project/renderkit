<?php

namespace Drupal\renderkit\EntityToEntities;

use Drupal\cfrreflection\Configurator\Configurator_CallbackMono;
use Drupal\renderkit\Configurator\Id\Configurator_FieldName;

class EntityToEntities_ParagraphsField extends EntityToEntitiesBase {

  /**
   * @var string
   */
  private $fieldName;

  /**
   * @CfrPlugin("paragraphsField", "Paragraphs field")
   *
   * @param string|null $entityType
   *   Contextual parameter
   * @param string|null $bundleName
   *   Contextual parameter
   *
   * @return \Drupal\cfrreflection\Configurator\Configurator_CallbackMono|null
   */
  public static function plugin($entityType = NULL, $bundleName = NULL) {

    if (!module_exists('paragraphs')) {
      return NULL;
    }

    return Configurator_CallbackMono::createFromClassName(
      self::class,
      new Configurator_FieldName(
        ['paragraphs'],
        $entityType,
        $bundleName));
  }

  /**
   * Constructor.
   *
   * @param string $fieldName
   */
  public function __construct($fieldName) {
    $this->fieldName = $fieldName;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType() {
    return 'paragraphs_item';
  }

  /**
   * {@inheritdoc}
   */
  public function entityGetRelated($entityType, $entity) {

    $paragraphs = [];
    foreach (field_get_items($entityType, $entity, $this->fieldName) ?: [] as $delta => $item) {

      if (FALSE === $paragraph = paragraphs_field_get_entity($item)) {
        continue;
      }

      try {
        $paragraph->setHostEntity($entityType, $entity);
      }
      catch (\Exception $e) {
        watchdog_exception(
          'renderkit',
          $e,
          'Exception trying to set host entity for paragraph item entity: '
          . '%type: !message in %function (line %line of %file).',
          [],
          WATCHDOG_WARNING);
        continue;
      }

      $paragraphs[$delta] = $paragraph;
    }

    return $paragraphs;
  }

}
