<?php

namespace Drupal\renderkit\EntityToEntities;

/**
 * Base class that collects target etids from each entity.
 */
abstract class EntityToEntities_TargetEtidsBase implements EntityToEntitiesInterface {

  /**
   * @param string $entityType
   * @param object $entity
   *
   * @return object[]
   *   Format: $[$delta] = $term
   */
  final public function entityGetRelated($entityType, $entity) {

    $targetEtids = $this->entityGetTargetEtids($entityType, $entity);

    $targetEntitiesByEtid = entity_load(
      $this->getTargetEntityType(),
      array_unique($targetEtids));

    $targetEntities = [];
    foreach ($targetEtids as $delta => $targetEtid) {
      if (isset($targetEntitiesByEtid[$targetEtid])) {
        $targetEntities[$delta] = $targetEntitiesByEtid[$targetEtid];
      }
    }

    return $targetEntities;
  }

  /**
   * @param string $entityType
   * @param array $entities
   *   Format: $[$delta0] = $entity
   *
   * @return object[][]
   *   Format: $[$delta0][$delta1] = $term
   */
  final public function entitiesGetRelated($entityType, array $entities) {

    $targetEtidsMap = [];
    $targetEtidss = [];
    foreach ($entities as $delta0 => $entity) {
      $targetEtidss[$delta0] = $tidsByDelta1 = $this->entityGetTargetEtids($entityType, $entity);
      $targetEtidsMap += array_fill_keys($tidsByDelta1, TRUE);
    }

    $targetEntitiesByEtid = entity_load(
      $this->getTargetEntityType(),
      array_keys($targetEtidsMap));

    $targetEntitiess = [];
    foreach ($targetEtidss as $delta0 => $targetEtids) {
      foreach ($targetEtids as $delta1 => $targetEtid) {
        if (isset($targetEntitiesByEtid[$targetEtid])) {
          $targetEntitiess[$delta0][$delta1] = $targetEntitiesByEtid[$targetEtid];
        }
      }
    }

    return $targetEntitiess;
  }

  /**
   * @param string $entityType
   * @param object $entity
   *
   * @return int[]
   *   Format: $[$delta] = $tid
   */
  abstract protected function entityGetTargetEtids($entityType, $entity);

}
