<?php

namespace Drupal\renderkit\EntityToEntities;

use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\renderkit\Configurator\Id\Configurator_FieldName;

class EntityToEntities_EntityReferenceField extends EntityToEntities_TargetEtidsBase {

  /**
   * @var string
   */
  private $fieldName;

  /**
   * @var string
   */
  private $targetType;

  /**
   * @CfrPlugin("entityReferenceField", "Entity reference field")
   *
   * @param string $entityType
   *   (optional) Contextual parameter.
   * @param string $bundleName
   *   (optional) Contextual parameter.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function createConfigurator($entityType = NULL, $bundleName = NULL) {
    $configurators = [new Configurator_FieldName(['entityreference'], $entityType, $bundleName)];
    $labels = [t('Entity reference field')];
    return Configurator_CallbackConfigurable::createFromClassStaticMethod(__CLASS__, 'create', $configurators, $labels);
  }

  /**
   * @param string $fieldName
   *
   * @return self|null
   */
  public static function create($fieldName) {
    $fieldInfo = field_info_field($fieldName);
    if (NULL === $fieldInfo) {
      throw new \InvalidArgumentException("Field '$fieldName' does not exist.");
    }
    if (!isset($fieldInfo['type'])) {
      throw new \InvalidArgumentException("Field '$fieldName' has no field type.");
    }
    if ($fieldInfo['type'] !== 'entityreference') {
      $typeExport = var_export($fieldInfo['type'], TRUE);
      throw new \InvalidArgumentException("Field type of '$fieldName' expected to be 'entityreference', $typeExport found instead.");
    }
    if (!isset($fieldInfo['settings']['target_type'])) {
      throw new \InvalidArgumentException("No target type in field info.");
    }
    return new self($fieldName, $fieldInfo['settings']['target_type']);
  }

  /**
   * Constructor.
   *
   * @param string $fieldName
   * @param string $targetType
   */
  public function __construct($fieldName, $targetType) {
    $this->fieldName = $fieldName;
    $this->targetType = $targetType;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType() {
    return $this->targetType;
  }

  /**
   * @param string $entityType
   * @param object $entity
   *
   * @return int[]
   *   Format: $[$delta] = $tid
   */
  protected function entityGetTargetEtids($entityType, $entity) {

    $targetEtids = [];
    foreach (field_get_items($entityType, $entity, $this->fieldName) ?: [] as $delta => $item) {

      if (empty($item['target_id'])) {
        continue;
      }

      $targetEtids[$delta] = $item['target_id'];
    }

    return $targetEtids;
  }

}
