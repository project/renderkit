<?php

namespace Drupal\renderkit\EntityToEntities;

use Drupal\cfrreflection\Configurator\Configurator_CallbackMono;
use Drupal\renderkit\Configurator\Id\Configurator_FieldName;

class EntityToEntities_TermReferenceField extends EntityToEntities_TargetEtidsBase {

  /**
   * @var string
   */
  private $fieldName;

  /**
   * @CfrPlugin("termReferenceField", "Term reference field")
   *
   * @param string|null $entityType
   *   Contextual parameter
   * @param string|null $bundleName
   *   Contextual parameter
   *
   * @return \Drupal\cfrreflection\Configurator\Configurator_CallbackMono
   */
  public static function plugin($entityType = NULL, $bundleName = NULL) {
    return Configurator_CallbackMono::createFromClassName(
      self::class,
      new Configurator_FieldName(
        ['taxonomy_term_reference'],
        $entityType,
        $bundleName));
  }

  /**
   * Constructor.
   *
   * @param string $fieldName
   */
  public function __construct($fieldName) {
    $this->fieldName = $fieldName;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType() {
    return 'taxonomy_term';
  }

  /**
   * @param string $entityType
   * @param object $entity
   *
   * @return int[]
   *   Format: $[$delta] = $tid
   */
  protected function entityGetTargetEtids($entityType, $entity) {

    $tids = [];
    foreach (field_get_items($entityType, $entity, $this->fieldName) ?: [] as $delta => $item) {

      if (empty($item['tid'])) {
        continue;
      }

      $tids[$delta] = $item['tid'];
    }

    return $tids;
  }

}
