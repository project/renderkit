<?php

namespace Drupal\renderkit\EntityToEntities;

/**
 * Represents a one-to-many relation between entities.
 */
interface EntityToEntitiesInterface {

  /**
   * Gets the entity type for target entities.
   *
   * @return string
   *   The target entity type.
   */
  public function getTargetEntityType();

  /**
   * Gets lists of related entities for each of the source entities.
   *
   * Implementations must be equivalent to the single-value version.
   *
   * @param string $entityType
   *   The source entity type.
   * @param object[] $entities
   *   List of source entities.
   *   Format: $[$sourceEntityDelta] = $sourceEntity
   *
   * @return object[][]
   *   Lists of target entities, using the same keys as $entities.
   *   Format: $[$sourceEntityDelta][$targetEntityDelta] = $targetEntity.
   */
  public function entitiesGetRelated($entityType, array $entities);

  /**
   * Gets a list of related entities a given source entity.
   *
   * @param string $entityType
   *   The source entity type.
   * @param object $entity
   *   The source entity.
   *
   * @return object[]
   *   List of target entities.
   *   Format: $[$targetEntityDelta] = $targetEntity
   */
  public function entityGetRelated($entityType, $entity);

}
