<?php

namespace Drupal\renderkit\BuildProvider;

use Drupal\cfrplugin\Util\UiUtil;
use Drupal\cfrplugin\UrlTokenPolicy\UrlTokenPolicy_Common;

/**
 * Component for a preview form / page.
 */
class BuildProvider_PreviewForm implements BuildProviderInterface {

  /**
   * State variable for recursion detection.
   *
   * @var true[]
   *   Format: $[$urlKey] = TRUE
   */
  private static $inProgress = [];

  /**
   * Url query key for the config.
   *
   * @var string
   */
  private $confKey;

  /**
   * Url query Key for the token.
   *
   * @var string
   */
  private $tokenKey;

  /**
   * Suffix to append to 'renderkit__', to produce a unique form id.
   *
   * @var string
   */
  private $formIdSuffix;

  /**
   * Url key to switch theme, or NULL to not show a theme switcher.
   *
   * @var string|null
   */
  private $themeSwitcherKey;

  /**
   * Static factory for the page in renderkit_ui.
   *
   * @return self
   */
  public static function createForUiPage() {
    return new self('', 'ui_preview_form', 'theme');
  }

  /**
   * Private constructor.
   *
   * Use static factories instead.
   *
   * @param string $query_key_prefix
   *   Key for the url query parameter to store the config.
   * @param string $form_id_suffix
   *   String to append to the form id to make it unique.
   * @param string|null $theme_switcher_key
   *   Url key to set a theme name.
   */
  private function __construct($query_key_prefix, $form_id_suffix, $theme_switcher_key = NULL) {
    $this->confKey = $query_key_prefix . 'plugin';
    $this->tokenKey = $query_key_prefix . 'token';
    $this->themeSwitcherKey = $theme_switcher_key;
    $this->formIdSuffix = $form_id_suffix;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    if (!empty(self::$inProgress[$this->confKey])) {
      return ['#markup' => t('Recursion detected.')];
    }
    self::$inProgress[$this->confKey] = TRUE;

    try {
      return $this->doBuild();
    }
    finally {
      unset(self::$inProgress[$this->confKey]);
    }
  }

  /**
   * Builds the form + preview area with some additional processing.
   *
   * @return array
   *   Render element.
   */
  private function doBuild() {
    $elements = $this->buildParts();
    $elements['#attached']['css'][] = drupal_get_path('module', 'renderkit')
      . '/css/renderkit.preview_form.css';
    foreach (element_children($elements) as $key) {
      $elements[$key]['#attributes']['class'][] = 'renderkit-preview-section';
    }
    return $elements;
  }

  /**
   * Builds the parts of the preview form.
   *
   * @return array
   *   Render element.
   */
  private function buildParts() {
    $elements = [];

    $settings = isset($_GET[$this->confKey])
      ? $_GET[$this->confKey]
      : NULL;

    $elements['form'] = $this->confBuildForm($settings);

    if (!$settings) {
      // No settings are provided. Only show the form.
      return $elements;
    }

    $token_policy = $this->getTokenPolicy();

    if (!$token_policy->queryIsSigned($_GET)) {
      $elements['sorry']['#markup'] = '<div>'
        . t('Token is wrong or missing.')
        . ' '
        . t('Please review and submit again.')
        . '</div>';
      return $elements;
    }

    try {
      $bp = BuildProvider::fromConf($settings);
    }
    catch (\Exception $e) {
      $elements['exception'] = UiUtil::displayException($e);
      $elements['exception'] += [
        '#type' => 'fieldset',
        '#title' => t('Exception'),
      ];
      return $elements;
    }

    $preview = $bp->build();

    if ($preview) {
      $elements['preview'] = [
        '#type' => 'fieldset',
        '#title' => t('Preview'),
        'content' => $preview,
        '#attributes' => ['class' => ['collapsible']],
      ];
    }

    // Make fieldsets collapsible.
    $elements['#attached']['library'][] = ['system', 'drupal.collapse'];

    return $elements;
  }

  /**
   * Builds the form to choose which plugin to preview.
   *
   * @param mixed $conf
   *   Configuration to use as default value.
   *
   * @return array
   *   Render element for the form.
   */
  private function confBuildForm($conf) {
    $form = [
      // Let this be converted to a form in '#pre_render'.
      '#type' => 'renderkit_generic_form',
      // Try to make the form id unique.
      '#form_id_suffix' => $this->formIdSuffix,
    ];

    $form['plugin'] = [
      '#title' => t('Element to preview'),
      /* @see cfrplugin_element_info() */
      '#type' => 'cfrplugin',
      '#cfrplugin_interface' => BuildProviderInterface::class,
      '#default_value' => $conf,
      '#required' => TRUE,
    ];

    if ($this->themeSwitcherKey !== NULL) {
      $theme = isset($_GET[$this->themeSwitcherKey]) ? $_GET[$this->themeSwitcherKey] : NULL;
      $form['theme'] = $this->buildThemeSelect($theme);
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Show'),
    ];

    // Register an object method as submit handler.
    // This is ok, because the object is easy to serialize.
    $form['#submit'][] = [$this, 'submit'];

    return $form;
  }

  /**
   * Builds the select element to choose a theme.
   *
   * @param string|null $theme_chosen
   *   Currently chosen theme.
   *
   * @return array|null
   */
  private function buildThemeSelect($theme_chosen) {

    $theme_default = variable_get('theme_default', 'bartik');

    $theme_options = [];
    foreach (list_themes() as $theme_name => $theme_object) {
      if (!drupal_theme_access($theme_object)) {
        continue;
      }
      $theme_options[$theme_name] = $theme_object->info['name'];
    }

    if (isset($theme_options[$theme_default])) {
      $theme_options[$theme_default] .= ' (' . t('default') . ')';
    }
    else {
      $theme_options[$theme_default] = $theme_default . ' (' . t('default') . ')';
    }

    $admin_theme = variable_get('admin_theme');
    if ($admin_theme !== NULL && isset($theme_options[$admin_theme])) {
      $theme_options[$admin_theme] .= ' (' . t('admin theme') . ')';
    }

    $element = [
      '#title' => t('Theme'),
      '#type' => 'select',
      '#options' => $theme_options,
      '#required' => FALSE,
      '#empty_value' => '',
      '#empty_option' => '- ' . t('Do not switch') . ' -',
    ];

    $element['#description'] = t('Choose a theme in which to render this preview page.');

    if ($theme_chosen !== NULL && isset($theme_options[$theme_chosen])) {
      $element['#default_value'] = $theme_chosen;
    }

    return $element;
  }

  /**
   * Form submit callback.
   *
   * @param array $form
   *   The form.
   * @param array $form_state
   *   The form state.
   */
  public function submit(array $form, array &$form_state) {
    $path = $_GET['q'];
    // Preserve existing, unrelated query parameters.
    $query = $_GET;
    $values = $form_state['values'];
    unset($query['q']);
    $query[$this->confKey] = $values['plugin'];

    if (isset($this->themeSwitcherKey)) {
      if (!empty($values['theme'])) {
        $query[$this->themeSwitcherKey] = $values['theme'];
      }
      else {
        unset($query[$this->themeSwitcherKey]);
      }
      $theme_token_policy = new UrlTokenPolicy_Common(
        $this->themeSwitcherKey . '_token',
        [$this->themeSwitcherKey]);
      $theme_token_policy->pathQuerySetToken($path, $query);
    }

    $token_policy = $this->getTokenPolicy();
    $token_policy->pathQuerySetToken($path, $query);

    $form_state['redirect'] = [$path, ['query' => $query]];
  }

  /**
   * Gets an object that can check if a query is properly signed.
   *
   * @return \Drupal\renderkit\UrlTokenPolicy\UrlTokenPolicyInterface
   */
  private function getTokenPolicy() {
    return new UrlTokenPolicy_Common(
      $this->tokenKey,
      [$this->confKey, $this->themeSwitcherKey]);
  }

}
