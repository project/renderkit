<?php

namespace Drupal\renderkit\BuildProvider;

use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\cfrreflection\Configurator\Configurator_CallbackMono;
use Drupal\renderkit\Configurator\Id\Configurator_ViewsDisplayId;
use Drupal\renderkit\LabeledFormat\LabeledFormatInterface;

/**
 * Component to show a views display.
 *
 * Any custom code using this should first check if views is enabled.
 * The plugin already does check this.
 */
class BuildProvider_ViewsDisplay implements BuildProviderInterface {

  /**
   * @var string
   */
  private $viewName;

  /**
   * @var string
   */
  private $displayId;

  /**
   * Format used to display the label of the view.
   *
   * @var \Drupal\renderkit\LabeledFormat\LabeledFormatInterface|null
   */
  private $labeledFormat;

  /**
   * @CfrPlugin("viewsDisplaySimple", @t("Views display"))
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   *   Configurator for this plugin, or NULL if not available.
   */
  public static function pluginSimple() {

    if (!module_exists('views')) {
      // The views module is not enabled.
      // Return NULL to hide this plugin.
      return NULL;
    }

    return Configurator_CallbackMono::createFromClassStaticMethod(
      self::class,
      /* @see createSimple() */
      'createSimple',
      new Configurator_ViewsDisplayId());
  }

  /**
   * Static factory using a combined string to identify the views display.
   *
   * @param string $viewNameWithDisplayId
   *   Format: "$view_name:$display_id".
   *
   * @return self|null
   *   Created instance, or NULL if the configuration is empty.
   */
  public static function createSimple($viewNameWithDisplayId) {
    list($view_name, $display_id) = explode(':', $viewNameWithDisplayId . ':');
    if ('' === $view_name || '' === $display_id) {
      return NULL;
    }
    // No further checking at this point.
    return new self($view_name, $display_id);
  }

  /**
   * @CfrPlugin("viewsDisplay", @t("Views display + label format"))
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   *   Configurator for this plugin, or NULL if not available.
   */
  public static function createConfigurator() {

    if (!module_exists('views')) {
      // The views module is not enabled.
      // Return NULL to hide this plugin.
      return NULL;
    }

    return Configurator_CallbackConfigurable::createFromClassStaticMethod(
      self::class,
      /* @see doCreate() */
      'doCreate',
      [
        new Configurator_ViewsDisplayId(),
        \cfrplugin()->interfaceGetOptionalConfigurator(
          LabeledFormatInterface::class),
      ],
      [
        t('Views display'),
        t('Label format'),
      ]);
  }

  /**
   * Static factory using a composite identifier and optional label format.
   *
   * @param string $viewNameWithDisplayId
   *   Format: "$view_name:$display_id".
   * @param \Drupal\renderkit\LabeledFormat\LabeledFormatInterface|null $labeledFormat
   *   (optional) Format used to display the label of the view.
   *
   * @return self|null
   *   Created instance, or NULL if the configuration is empty.
   */
  public static function doCreate($viewNameWithDisplayId, LabeledFormatInterface $labeledFormat = NULL) {
    list($view_name, $display_id) = explode(':', $viewNameWithDisplayId . ':');
    if ('' === $view_name || '' === $display_id) {
      return NULL;
    }
    // No further checking at this point.
    return new self($view_name, $display_id, $labeledFormat);
  }

  /**
   * Constructor.
   *
   * @param string $viewName
   *   The views view machine name.
   * @param string $displayId
   *   The views display id.
   * @param \Drupal\renderkit\LabeledFormat\LabeledFormatInterface|null $labeledFormat
   *   (optional) Format used to display the label of the view.
   */
  public function __construct($viewName, $displayId, LabeledFormatInterface $labeledFormat = NULL) {
    $this->viewName = $viewName;
    $this->displayId = $displayId;
    $this->labeledFormat = $labeledFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $view = \views_get_view($this->viewName);
    if (NULL === $view) {
      return [];
    }
    $success = $view->set_display($this->displayId);
    if (FALSE === $success) {
      return [];
    }
    // Let the exposed form stay on the current page.
    $view->override_url = $this->getOverrideUrl();
    $markup = $view->preview();
    if (FALSE === $markup) {
      return [];
    }
    $build = ['#markup' => $markup];
    if (NULL === $this->labeledFormat) {
      return $build;
    }
    $label = $view->get_title();
    if (empty($label)) {
      return $build;
    }
    return $this->labeledFormat->buildAddLabel($build, $label);
  }

  /**
   * Gets a url to use e.g. as destination for the exposed form.
   *
   * @return string
   *   A complete url.
   */
  private function getOverrideUrl() {
    $path = $_GET['q'];
    $query = $_GET;
    unset($query['q']);
    return url($path, ['query' => $query, 'absolute' => TRUE]);
  }

}
