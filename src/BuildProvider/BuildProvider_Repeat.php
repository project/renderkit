<?php

namespace Drupal\renderkit\BuildProvider;

use Drupal\cfrapi\Configurator\Configurator_IntegerInRange;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\renderkit\ListFormat\ListFormat;
use Drupal\renderkit\ListFormat\ListFormatInterface;

class BuildProvider_Repeat implements BuildProviderInterface {

  /**
   * @var \Drupal\renderkit\BuildProvider\BuildProviderInterface
   */
  private $provider;

  /**
   * @var \Drupal\renderkit\ListFormat\ListFormatInterface
   */
  private $listFormat;

  /**
   * @var int
   */
  private $n;

  /**
   * @CfrPlugin("repeat", "Repeat the same element")
   *
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function plugin(CfrContextInterface $context = NULL) {
    return Configurator_CallbackConfigurable::createFromClassName(
      __CLASS__,
      [
        BuildProvider::configurator($context),
        ListFormat::configurator($context),
        new Configurator_IntegerInRange(1, 100),
      ],
      [
        t('Build provider'),
        t('List format'),
        t('Number of repetitions'),
      ]);
  }

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\BuildProvider\BuildProviderInterface $provider
   * @param \Drupal\renderkit\ListFormat\ListFormatInterface $listFormat
   * @param int $n
   */
  public function __construct(BuildProviderInterface $provider, ListFormatInterface $listFormat = NULL, $n) {
    $this->provider = $provider;
    $this->listFormat = $listFormat;
    $this->n = $n;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = $this->provider->build();
    if ([] === $build) {
      return [];
    }
    $builds = array_fill(0, $this->n, $build);
    if ([] === $builds) {
      return [];
    }
    if (NULL !== $this->listFormat) {
      $builds = $this->listFormat->buildList($builds);
    }
    return $builds;
  }

}
