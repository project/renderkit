<?php

namespace Drupal\renderkit\BuildProvider;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\renderkit\IdToConfigurator\IdToConfigurator_DsLayoutRegions;

/**
 * Shows elements in a Display Suite layout.
 */
class BuildProvider_DsLayout implements BuildProviderInterface {

  /**
   * @var string
   */
  private $layoutName;

  /**
   * @var \Drupal\renderkit\BuildProvider\BuildProviderInterface[]|null[]
   */
  private $regionDisplays;

  /**
   * @CfrPlugin("dsLayout", "Ds layout")
   *
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *   (optional) Context to restrict available options.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   *   Configurator for this plugin, or NULL if not available.
   */
  public static function plugin(CfrContextInterface $context = NULL) {
    $configurator = IdToConfigurator_DsLayoutRegions::createDrilldown(
      BuildProvider::optionalConfigurator());
    if (!$configurator) {
      return NULL;
    }
    return $configurator->withValueConstructor(self::class);
  }

  /**
   * Constructor.
   *
   * @param string $layoutName
   *   Layout name.
   * @param \Drupal\renderkit\BuildProvider\BuildProviderInterface[]|null[] $regionDisplays
   *   Display for each region.
   */
  public function __construct(string $layoutName, array $regionDisplays) {
    $this->layoutName = $layoutName;
    $this->regionDisplays = $regionDisplays;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $element = [
      /* @see \_renderkit_ds_layout_pre_render() */
      '#type' => 'renderkit_ds_layout',
      '#layout_name' => $this->layoutName,
    ];
    foreach ($this->regionDisplays as $regionName => $display) {
      $element[$regionName] = $display ? $display->build() : [];
    }
    return $element;
  }

}
