<?php

namespace Drupal\renderkit\BuildProvider;

class BuildProvider_FixedBuild implements BuildProviderInterface {

  /**
   * @var array
   */
  private $build;

  /**
   * Constructor.
   *
   * @param array $build
   */
  public function __construct(array $build) {
    $this->build = $build;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->build;
  }

}
