<?php

namespace Drupal\renderkit\Legend;

use Drupal\cfrapi\Legend\LegendInterface;

/**
 * Legend to choose a ds layout name.
 *
 * @see \_ds_field_ui_table_layouts()
 */
class Legend_DsLayout implements LegendInterface {

  /**
   * Constructor.
   *
   * @throws \Exception
   *   If module 'ds' does not exist.
   */
  public function __construct() {
    // Checking function_exists() is faster than module_exists().
    if (!function_exists('ds_get_layout_info')) {
      throw new \Exception('Display Suite is not installed.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectOptions() {
    $options = [];
    foreach (ds_get_layout_info() as $layout_name => $layout) {
      if (!isset($layout['regions'])) {
        $x = 5;
      }
      if (array_keys($layout['regions']) === 'ds_hidden') {
        $x = 5;
      }
      if (!isset($layout['module'])) {
        $optgroup = t('Display Suite');
      }
      else {
        $optgroup = drupal_ucfirst($layout['module']);
      }
      $options[$optgroup][$layout_name] = $layout['label'];
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetLabel($layout_name) {
    $layouts = ds_get_layout_info();
    if (isset($layouts[$layout_name]['label'])) {
      return $layouts[$layout_name]['label'];
    }
    if (!isset($layouts[$layout_name])) {
      return NULL;
    }
    return $layout_name;
  }

  /**
   * {@inheritdoc}
   */
  public function idIsKnown($layout_name) {
    $layouts = ds_get_layout_info();
    return isset($layouts[$layout_name]);
  }

}
