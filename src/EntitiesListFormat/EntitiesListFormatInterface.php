<?php

namespace Drupal\renderkit\EntitiesListFormat;

/**
 * Object to show a list of entities.
 */
interface EntitiesListFormatInterface {

  /**
   * Displays entities as a list, e.g. as a table.
   *
   * @param string $entityType
   *   The entity type.
   * @param object[] $entities
   *   The list of entities.
   *   Keys can be numeric/serial or string/associative.
   *   Keys should NOT begin with '#'.
   *
   * @return array
   *   A render array.
   */
  public function entitiesBuildList($entityType, array $entities);

}
