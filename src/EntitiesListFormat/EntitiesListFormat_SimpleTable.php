<?php

namespace Drupal\renderkit\EntitiesListFormat;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrreflection\Configurator\Configurator_CallbackMono;
use Drupal\renderkit\EntityDisplay\EntityDisplay;

class EntitiesListFormat_SimpleTable implements EntitiesListFormatInterface {

  /**
   * @var \Drupal\renderkit\EntityDisplay\EntityDisplayInterface[]
   */
  private $columnDisplays;

  /**
   * @CfrPlugin("simpleTable", "Simple table")
   *
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function createConfigurator(CfrContextInterface $context = NULL) {
    return Configurator_CallbackMono::createFromClassName(
      self::class,
      EntityDisplay::sequenceConfigurator($context));
  }

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface[] $columnDisplays
   */
  public function __construct(array $columnDisplays) {
    $this->columnDisplays = $columnDisplays;
  }

  /**
   * {@inheritdoc}
   */
  public function entitiesBuildList($entityType, array $entities) {

    $rows_sparse = [];
    foreach ($this->columnDisplays as $colKey => $columnDisplay) {
      foreach ($columnDisplay->buildEntities($entityType, $entities) as $delta => $build) {
        $rows_sparse[$delta][$colKey] = drupal_render($build);
      }
    }

    // Row filled with empty strings.
    $empty_row = array_fill_keys(
      array_keys($this->columnDisplays),
      '');

    $rows = [];
    foreach (array_intersect_key($entities, $rows_sparse) as $delta => $entity) {
      $rows[] = array_replace($empty_row, $rows_sparse[$delta]);
    }

    return [
      /* @see theme_table() */
      '#theme' => 'table',
      '#rows' => $rows,
    ];
  }

}
