<?php

namespace Drupal\renderkit\EntitiesListFormat;

use Drupal\renderkit\EntityDisplay\EntityDisplayInterface;
use Drupal\renderkit\ListFormat\ListFormatInterface;

/**
 * @CfrPlugin("listFormat", "Entity display + list format")
 */
class EntitiesListFormat_ListFormat implements EntitiesListFormatInterface {

  /**
   * @var \Drupal\renderkit\EntityDisplay\EntityDisplayInterface
   */
  private $entityDisplay;

  /**
   * @var \Drupal\renderkit\ListFormat\ListFormatInterface|null
   */
  private $listFormat;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface $entityDisplay
   * @param \Drupal\renderkit\ListFormat\ListFormatInterface|null $listFormat
   */
  public function __construct(EntityDisplayInterface $entityDisplay, ListFormatInterface $listFormat = NULL) {
    $this->entityDisplay = $entityDisplay;
    $this->listFormat = $listFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function entitiesBuildList($entityType, array $entities) {
    $builds = $this->entityDisplay->buildEntities($entityType, $entities);
    if (NULL === $this->listFormat) {
      return $builds;
    }
    return $this->listFormat->buildList($builds);
  }

}
