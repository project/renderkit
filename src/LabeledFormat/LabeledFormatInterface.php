<?php

namespace Drupal\renderkit\LabeledFormat;

/**
 * Formats an element with label.
 *
 * @todo Replace this with a more elegant interface.
 */
interface LabeledFormatInterface {

  /**
   * Formats an element with label.
   *
   * @param array $build
   *   Content render element.
   * @param string $label
   *   A label / title.
   *
   * @return array
   *   Transformed or wrapped render array with label.
   */
  public function buildAddLabel(array $build, $label);

}
