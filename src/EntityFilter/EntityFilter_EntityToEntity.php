<?php

namespace Drupal\renderkit\EntityFilter;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\renderkit\EntityToEntity\EntityToEntity;
use Drupal\renderkit\EntityToEntity\EntityToEntityInterface;

class EntityFilter_EntityToEntity implements EntityFilterInterface {

  /**
   * @var \Drupal\renderkit\EntityToEntity\EntityToEntityInterface
   */
  private $entityToEntity;

  /**
   * @var \Drupal\renderkit\EntityFilter\EntityFilterInterface
   */
  private $targetEntityFilter;

  /**
   * @CfrPlugin("related", "Related entity")
   *
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function plugin(CfrContextInterface $context = NULL) {
    return Configurator_CallbackConfigurable::createFromClassName(
      __CLASS__,
      [
        EntityToEntity::configurator($context),
        // This one is without context, because we don't know the target entity type.
        cfrplugin()->interfaceGetConfigurator(EntityFilterInterface::class),
      ],
      [
        t('Entity relation'),
        t('Entity filter for target entity'),
      ]
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityToEntity\EntityToEntityInterface $entityToEntity
   * @param \Drupal\renderkit\EntityFilter\EntityFilterInterface $targetEntityFilter
   */
  public function __construct(EntityToEntityInterface $entityToEntity, EntityFilterInterface $targetEntityFilter) {
    $this->entityToEntity = $entityToEntity;
    $this->targetEntityFilter = $targetEntityFilter;
  }

  /**
   * {@inheritdoc}
   */
  public function entitiesFilterDeltas($entityType, array $entities) {

    $targetEntities = $this->entityToEntity->entitiesGetRelated(
      $entityType,
      $entities);

    if ([] === $targetEntities) {
      // Reject all, if no target entity found.
      return [];
    }

    return $this->targetEntityFilter->entitiesFilterDeltas(
      $this->entityToEntity->getTargetType(),
      $targetEntities);
  }

}
