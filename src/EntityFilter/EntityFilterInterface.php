<?php

namespace Drupal\renderkit\EntityFilter;

/**
 * Interface for entity filters.
 *
 * This concept is parallel to "entity condition", and there are adapters in
 * both directions.
 *
 * @see \Drupal\renderkit\EntityCondition\EntityConditionInterface
 */
interface EntityFilterInterface {

  /**
   * Filters the entities based on a condition.
   *
   * The signature and return type guarantee that no new entities are added by
   * a misbehaved implementation.
   *
   * @param string $entityType
   *   Entity type.
   * @param object[] $entities
   *   List of entities.
   *   Format: $[$delta] = $entity.
   *
   * @return string[]
   *   Format: $[] = $delta
   *   A filtered subset of the array keys of the $entities argument.
   */
  public function entitiesFilterDeltas($entityType, array $entities);

}
