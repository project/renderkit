<?php

namespace Drupal\renderkit\EntityFilter;

/**
 * @CfrPlugin(
 *   id = "negation",
 *   label = @t("Negation")
 * )
 */
class EntityFilter_Negation implements EntityFilterInterface {

  /**
   * @var \Drupal\renderkit\EntityFilter\EntityFilterInterface
   */
  private $negatedFilter;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityFilter\EntityFilterInterface $negatedFilter
   */
  public function __construct(EntityFilterInterface $negatedFilter) {
    $this->negatedFilter = $negatedFilter;
  }

  /**
   * {@inheritdoc}
   */
  public function entitiesFilterDeltas($entityType, array $entities) {
    foreach ($this->negatedFilter->entitiesFilterDeltas($entityType, $entities) as $delta) {
      unset($entities[$delta]);
    }
    return array_keys($entities);
  }

}
