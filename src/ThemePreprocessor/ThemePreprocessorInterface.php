<?php

namespace Drupal\renderkit\ThemePreprocessor;

/**
 * Callback object to alter variables sent to a theme hook.
 *
 * @todo This is not used. Deprecate and remove.
 */
interface ThemePreprocessorInterface {

  /**
   * Magic callback method.
   *
   * The signature is the same as for regular hook_preprocess() functions.
   *
   * @param array $variables
   *   Variables to be altered.
   * @param string $hook
   *   The theme hook. This is always passed in to preprocess functions.
   *
   * @see \hook_preprocess()
   * @see \theme()
   */
  public function __invoke(&$variables, $hook);

}
