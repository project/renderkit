<?php

namespace Drupal\renderkit\EntityBuildProcessor;

use Drupal\renderkit\EntityDisplay\EntityDisplayInterface;

/**
 * Processor which prepends or appends content if the main element is not empty.
 *
 * In the current implementation, only an empty render element counts as empty.
 *
 * A future version might check for empty string after the element is rendered.
 */
class EntityBuildProcessor_PrependOrAppend extends EntityBuildProcessorBase {

  /**
   * @var \Drupal\renderkit\EntityDisplay\EntityDisplayInterface
   */
  private $entityDisplay;

  /**
   * @var bool
   *   TRUE to prepend, FALSE to append.
   */
  private $prepend;

  /**
   * @CfrPlugin("prepend", "Prepend content, if not empty")
   *
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface $entityDisplayToPrepend
   *
   * @return self
   */
  public static function prepend(EntityDisplayInterface $entityDisplayToPrepend) {
    return new self($entityDisplayToPrepend, TRUE);
  }

  /**
   * @CfrPlugin("append", "Append content, if not empty")
   *
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface $entityDisplayToAppend
   *
   * @return self
   */
  public static function append(EntityDisplayInterface $entityDisplayToAppend) {
    return new self($entityDisplayToAppend, FALSE);
  }

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityDisplay\EntityDisplayInterface $entityDisplay
   *   Entity display to build the content to prepend or append.
   * @param bool $prepend
   *   TRUE to prepend, FALSE to append.
   */
  public function __construct(EntityDisplayInterface $entityDisplay, $prepend) {
    $this->entityDisplay = $entityDisplay;
    $this->prepend = $prepend;
  }

  /**
   * {@inheritdoc}
   */
  public function processEntityBuild(array $build, $entity_type, $entity) {

    if (!$build) {
      // Prepend nothing, if the display is empty.
      return [];
    }

    $element = $this->entityDisplay->buildEntity($entity_type, $entity);

    if (!$element) {
      // Nothing to prepend or append.
      // Return the original element.
      return $build;
    }

    return $this->prepend
      ? [
        'prepend' => $element,
        'original' => $build,
      ]
      : [
        'original' => $build,
        'append' => $element,
      ];
  }

}
