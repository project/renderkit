<?php

namespace Drupal\renderkit\EntityBuildProcessor;

use Drupal\renderkit\EntityImage\EntityImageInterface;
use Drupal\renderkit\Html\HtmlAttributesInterface;
use Drupal\renderkit\Html\HtmlTagTrait;

/**
 * @CfrPlugin(
 *   id = "entityBackgroundImageWrapper",
 *   label = @t("Entity background image wrapper")
 * )
 */
class EntityBuildProcessor_Wrapper_BackgroundImage extends EntityBuildProcessorBase implements HtmlAttributesInterface {

  use HtmlTagTrait;

  /**
   * @var \Drupal\renderkit\EntityImage\EntityImageInterface
   */
  private $imageProvider;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\EntityImage\EntityImageInterface $imageProvider
   */
  public function __construct(EntityImageInterface $imageProvider) {
    $this->imageProvider = $imageProvider;
  }

  /**
   * {@inheritdoc}
   */
  public function processEntityBuild(array $build, $entity_type, $entity) {
    $imageDisplay = $this->imageProvider->buildEntity($entity_type, $entity);
    $build = $this->buildContainer() + ['content' => $build];
    if (isset($imageDisplay['#path'])) {
      $imageUrl = file_create_url($imageDisplay['#path']);
      $build['#attributes']['style'] = 'background-image: url(' . json_encode($imageUrl) . ')';
    }
    return $build;
  }

}
