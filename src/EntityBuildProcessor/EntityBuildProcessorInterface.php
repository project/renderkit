<?php

namespace Drupal\renderkit\EntityBuildProcessor;

/**
 * Transforms a render element using data from an entity.
 */
interface EntityBuildProcessorInterface {

  /**
   * Transforms multiple elements at once.
   *
   * The implementation must be equivalent with the single-element method.
   *
   * @param array[] $builds
   *   Render elements to transform.
   *   Array keys can be serial or associative.
   * @param string $entity_type
   *   An entity type for the list of entities.
   * @param object[] $entities
   *   List of entities that should inform the transformation.
   *   Array keys must be the same as for $builds.
   *
   * @return array[]
   *   Transformed render elements.
   *   The array keys must be the same as for $builds and $entities.
   */
  public function processEntitiesBuilds(array $builds, $entity_type, array $entities);

  /**
   * Transforms a single element.
   *
   * The implementation must be equivalent with the multiple-elements method.
   *
   * @param array $build
   *   The render array produced by the decorated display handler.
   * @param string $entity_type
   *   Entity type.
   * @param object $entity
   *   Entity that should inform the transformation.
   *
   * @return array
   *   Transformed render element.
   */
  public function processEntityBuild(array $build, $entity_type, $entity);

}
