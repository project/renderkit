<?php

namespace Drupal\renderkit\ListFormat;

/**
 * List format which concatenates the elements as-is, without any wrappers.
 *
 * @CfrPlugin("passthru", "Passthru")
 */
class ListFormat_Passthru implements ListFormatInterface {

  /**
   * {@inheritdoc}
   */
  public function buildList(array $builds) {
    // Filter out keys beginning with '#'.
    foreach (element_properties($builds) as $key) {
      // This should not happen, but...
      unset($builds[$key]);
    }
    return $builds;
  }

}
