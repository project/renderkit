<?php

namespace Drupal\renderkit\ListFormat;

/**
 * Formats a list of elements.
 */
interface ListFormatInterface {

  /**
   * Formats a list of elements.
   *
   * @param array[] $builds
   *   Render elements for list items.
   *   Array keys can be serial or associative.
   *   Array keys must NOT begin with '#'.
   *
   * @return array
   *   Render element for the list.
   */
  public function buildList(array $builds);

}
