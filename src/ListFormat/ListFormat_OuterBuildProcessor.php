<?php

namespace Drupal\renderkit\ListFormat;

use Drupal\renderkit\BuildProcessor\BuildProcessorInterface;

/**
 * @CfrPlugin("outerBuildProcessor", "Outer build processor")
 */
class ListFormat_OuterBuildProcessor implements ListFormatInterface {

  /**
   * @var \Drupal\renderkit\BuildProcessor\BuildProcessorInterface
   */
  private $buildProcessor;

  /**
   * @var \Drupal\renderkit\ListFormat\ListFormatInterface|null
   */
  private $decorated;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\BuildProcessor\BuildProcessorInterface $outerBuildProcessor
   * @param \Drupal\renderkit\ListFormat\ListFormatInterface|null $decoratedListFormat
   */
  public function __construct(BuildProcessorInterface $outerBuildProcessor, ListFormatInterface $decoratedListFormat = NULL) {
    $this->buildProcessor = $outerBuildProcessor;
    $this->decorated = $decoratedListFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function buildList(array $builds) {
    if (NULL !== $this->decorated) {
      $builds = $this->decorated->buildList($builds);
    }
    return $this->buildProcessor->process($builds);
  }

}
