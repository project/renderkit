<?php

namespace Drupal\renderkit\ListFormat;

use Drupal\renderkit\LabeledListFormat\LabeledListFormatInterface;

class ListFormat_Labeled implements ListFormatInterface {

  /**
   * @var \Drupal\renderkit\LabeledListFormat\LabeledListFormatInterface
   */
  private $labeledListFormat;

  /**
   * @var string
   */
  private $label;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\LabeledListFormat\LabeledListFormatInterface $labeledListFormat
   * @param string $label
   */
  public function __construct(LabeledListFormatInterface $labeledListFormat, $label) {
    $this->labeledListFormat = $labeledListFormat;
    $this->label = $label;
  }

  /**
   * {@inheritdoc}
   */
  public function buildList(array $builds) {
    return $this->labeledListFormat->build($builds, $this->label);
  }

}
