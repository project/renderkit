<?php

namespace Drupal\renderkit\ListFormat;

class ListFormat_ElementDefaults implements ListFormatInterface {

  /**
   * @var array
   */
  private $elementDefaults;

  /**
   * Constructor.
   *
   * @param array $elementDefaults
   */
  public function __construct(array $elementDefaults) {
    $this->elementDefaults = $elementDefaults;
  }

  /**
   * {@inheritdoc}
   */
  public function buildList(array $builds) {
    return $builds + $this->elementDefaults;
  }

}
