<?php

namespace Drupal\renderkit\ImagesDisplay;

/**
 * Formats a list of images, e.g. as some kind of gallery.
 */
interface ImagesDisplayInterface {

  /**
   * Formats a list of images.
   *
   * @param array[] $images
   *   List of images, as render elements with '#theme' => 'image'.
   *   Array keys can be serial or associative.
   *   Array keys should NOT begin with '#'.
   *
   * @return array
   *   Render element showing all the images.
   */
  public function buildImages(array $images);

}
