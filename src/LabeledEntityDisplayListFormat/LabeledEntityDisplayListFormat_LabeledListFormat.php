<?php

namespace Drupal\renderkit\LabeledEntityDisplayListFormat;

use Drupal\renderkit\LabeledListFormat\LabeledListFormatInterface;

/**
 * Implementation that does not use the entity.
 *
 * @CfrPlugin(
 *   id = "withoutEntity",
 *   label = "Without entity",
 *   inline = true
 * )
 */
class LabeledEntityDisplayListFormat_LabeledListFormat implements LabeledEntityDisplayListFormatInterface {

  /**
   * @var \Drupal\renderkit\LabeledListFormat\LabeledListFormatInterface
   */
  private $labeledListFormat;

  /**
   * Constructor.
   *
   * @param \Drupal\renderkit\LabeledListFormat\LabeledListFormatInterface $labeledListFormat
   */
  public function __construct(LabeledListFormatInterface $labeledListFormat) {
    $this->labeledListFormat = $labeledListFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $builds, $entityType, $entity, $label) {
    return $this->labeledListFormat->build($builds, $label);
  }

}
