<?php

namespace Drupal\renderkit\LabeledEntityDisplayListFormat;

/**
 * Displays a list of elements with a label, informed by an entity.
 *
 * @todo This interface does not seem very useful.
 */
interface LabeledEntityDisplayListFormatInterface {

  /**
   * Displays a list of elements with a label, informed by an entity.
   *
   * @param array[] $builds
   *   Render elements for list items.
   * @param string $entityType
   *   Entity type.
   * @param object $entity
   *   Entity that should inform how the list + label are displayed.
   * @param string $label
   *   A label, e.g. for a field or field group.
   *
   * @return array
   *   Combined render array.
   */
  public function build(array $builds, $entityType, $entity, $label);

}
