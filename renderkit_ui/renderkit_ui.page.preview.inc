<?php

/**
 * @file
 * Contains a page callback.
 */

use Drupal\renderkit\BuildProvider\BuildProviderInterface;
use Drupal\renderkit_ui\ConfDisplay\ConfDisplay_BuildProviderPreview;
use Drupal\renderkit_ui\UiElement\UiElement_ThemeSwitchingDemoPage;

/**
 * Page callback for the preview builder.
 *
 * Most of the logic is inside a class.
 *
 * @return array
 *   Render element for the page.
 */
function _renderkit_ui_preview_page() {
  $ui_element = new UiElement_ThemeSwitchingDemoPage(
    BuildProviderInterface::class,
    new ConfDisplay_BuildProviderPreview());
  return $ui_element->build();
}
