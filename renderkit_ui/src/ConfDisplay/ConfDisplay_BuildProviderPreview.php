<?php

namespace Drupal\renderkit_ui\ConfDisplay;

use Drupal\cfrplugin\ConfDisplay\ConfDisplayInterface;
use Drupal\cfrplugin\Util\UiUtil;
use Drupal\renderkit\BuildProvider\BuildProvider;

/**
 * Display that shows a preview of the "Build provider" component.
 *
 * This assumes that the $conf is for a BuiLdProvider component.
 */
class ConfDisplay_BuildProviderPreview implements ConfDisplayInterface {

  /**
   * @var bool|null
   */
  private $collapsed;

  /**
   * @param bool|null $collapsed
   *   TRUE for collapsed, FALSE for collapsible, NULL for always open.
   */
  public function __construct($collapsed = NULL) {
    $this->collapsed = $collapsed;
  }

  /**
   * {@inheritdoc}
   */
  public function buildWithConf($conf) {

    try {
      $bp = BuildProvider::fromConf($conf);
    }
    catch (\Exception $e) {
      $elements['exception'] = UiUtil::displayException($e);
      $elements['exception'] += [
        '#type' => 'fieldset',
        '#title' => t('Exception'),
      ];
      return $elements;
    }

    $preview = $bp->build();

    $fieldset = [
      '#type' => 'fieldset',
      '#title' => $preview
        ? t('Preview')
        : t('Preview (empty)'),
      'content' => $preview ?: [],
      '#attributes' => ['class' => ['collapsible']],
    ];

    if ($this->collapsed) {
      // Collapsed.
      $fieldset['#attributes']['class'] = ['collapsible', 'collapsed'];
      $fieldset['#attached']['library'][] = ['system', 'drupal.collapse'];
    }
    elseif (FALSE === $this->collapsed) {
      // Collapsible.
      $fieldset['#attributes']['class'] = ['collapsible'];
      $fieldset['#attached']['library'][] = ['system', 'drupal.collapse'];
    }

    $elements['preview'] = $fieldset;

    return $elements;
  }

}
