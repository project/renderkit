<?php

namespace Drupal\renderkit_ui\UiElement;

use Drupal\cfrplugin\UiElement\UiElement_DemoPage;
use Drupal\cfrplugin\UrlTokenPolicy\UrlTokenPolicy_Common;

/**
 * Demo page with an additional theme-switching option.
 */
class UiElement_ThemeSwitchingDemoPage extends UiElement_DemoPage {

  /**
   * {@inheritdoc}
   */
  protected function confBuildForm($conf) {
    $form = parent::confBuildForm($conf);
    $form['theme'] = $this->buildThemeSelect();
    return $form;
  }

  /**
   * Builds the select element to choose a theme.
   *
   * @return array
   *   Form element with '#type' => 'select'.
   */
  private function buildThemeSelect() {
    $theme_chosen = isset($_GET['theme']) ? $_GET['theme'] : NULL;

    $theme_default = variable_get('theme_default', 'bartik');

    $theme_options = [];
    foreach (list_themes() as $theme_name => $theme_object) {
      if (!drupal_theme_access($theme_object)) {
        continue;
      }
      $theme_options[$theme_name] = $theme_object->info['name'];
    }

    if (isset($theme_options[$theme_default])) {
      $theme_options[$theme_default] .= ' (' . t('default') . ')';
    }
    else {
      $theme_options[$theme_default] = $theme_default . ' (' . t('default') . ')';
    }

    $admin_theme = variable_get('admin_theme');
    if ($admin_theme !== NULL && isset($theme_options[$admin_theme])) {
      $theme_options[$admin_theme] .= ' (' . t('admin theme') . ')';
    }

    $element = [
      '#title' => t('Theme'),
      '#type' => 'select',
      '#options' => $theme_options,
      '#required' => FALSE,
      '#empty_value' => '',
      '#empty_option' => '- ' . t('Do not switch') . ' -',
    ];

    $element['#description'] = t('Choose a theme in which to render this preview page.');

    if ($theme_chosen !== NULL && isset($theme_options[$theme_chosen])) {
      $element['#default_value'] = $theme_chosen;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function submit(array $form, array &$form_state) {
    // The parent implementation sets a signed redirect url.
    parent::submit($form, $form_state);
    if (isset($form_state['values']['theme'])) {
      // The parameters are preserved in the parent method.
      $form_state['redirect'][1]['query']['theme'] = $form_state['values']['theme'];
    }
    static::getThemeTokenPolicy()->pathQuerySetToken(
      $form_state['redirect'][0],
      $form_state['redirect'][1]['query']);
  }

  /**
   * Gets a token policy object specifically for the 'theme' parameter.
   *
   * @return \Drupal\cfrplugin\UrlTokenPolicy\UrlTokenPolicyInterface
   *   Token policy object.
   */
  public static function getThemeTokenPolicy() {
    return new UrlTokenPolicy_Common(
      'theme_token',
      ['theme']);
  }

}
